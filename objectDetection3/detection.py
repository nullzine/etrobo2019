import cv2
import numpy as np
import matplotlib.pyplot as plt

full_images = []
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_144913.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_145446.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_150850.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_151305.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_151843.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_152311.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_152735.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_154329.jpg'))
full_images.append(cv2.imread('C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\camSysDataset\\7\\dataset_20190629_154755.jpg'))

for i in range(len(full_images)):
    full_images[i] = cv2.cvtColor(full_images[i], cv2.COLOR_BGR2RGB)

face_images = []
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\yellow1.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\yellow2.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\yellow3.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\yellow4.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\red1.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\red2.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\red3.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\red4.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\blue1.jpg'))
face_images.append(cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\blue2.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\blue3.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\blue4.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\green1.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\green2.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\green3.jpg'))
face_images.append( cv2.imread('C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\objectDetection3\\green4.jpg'))

for i in range(len(face_images)):
    face_images[i]=cv2.cvtColor(face_images[i], cv2.COLOR_BGR2RGB)

 
#methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR','cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

formula = 'cv2.TM_CCOEFF_NORMED'

for idx,full_img in enumerate(full_images):

    full_copy = full_img.copy()

    for face_img in face_images:
        height, width,channels = face_img.shape

        method = eval(formula)
        res = cv2.matchTemplate(full_copy,face_img,method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        top_left = max_loc

        bottom_right = (top_left[0] + width, top_left[1] + height)
        cv2.rectangle(full_copy,top_left, bottom_right, 255, 10)

    plt.subplot(121)
    plt.imshow(res)
    plt.title('Result of Template Matching')

    plt.subplot(122)
    plt.imshow(full_copy)
    plt.title('Detected Point')

    plt.suptitle(formula)
    #plt.show()
    plt.savefig("figure"+str(idx)+".png")

'''
for m in methods:
    
    full_copy = full_img.copy()
    
    method = eval(m)
 
    res = cv2.matchTemplate(full_copy,face_img,method)
    
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    
 
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        top_left = min_loc    
    else:
        top_left = max_loc
        
    bottom_right = (top_left[0] + width, top_left[1] + height)
 
    cv2.rectangle(full_copy,top_left, bottom_right, 255, 10)
 
    plt.subplot(121)
    plt.imshow(res)
    plt.title('Result of Template Matching')
    
    plt.subplot(122)
    plt.imshow(full_copy)
    plt.title('Detected Point')
 
    plt.suptitle(m)
    
    
    plt.show()
    print('\n')
    print('\n')
'''