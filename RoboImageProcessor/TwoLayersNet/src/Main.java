import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;
import java.io.*;
import java.util.function.Function;
import java.util.stream.*;
import java.util.zip.GZIPInputStream;

public class Main {

    public static void main(String[] args) {
        new Main().run();
        //new Main().test();
    }

    private void test() {
        float[][] t = new float[3][4];
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j < t[i].length; j++) {
                t[i][j] = i * t[i].length + j;
            }
        }
        Mat mat = new Mat(t);
        System.out.println(mat);
        mat = mat.reshape(4, 3);
        System.out.println(mat);
    }

    private void run() {
        MNIST mnist = new MNIST("mnist");
        Mat x_train = new Mat(mnist.getTrainImageFloat(255));
        Mat t_train = new Mat(mnist.getTrainLabelOneHotFloat());
        Mat x_test = new Mat(mnist.getTestImageFloat(255));
        Mat t_test = new Mat(mnist.getTestLabelOneHotFloat());
        System.out.println(new Vec(i2f(x_train.shape())));
        System.out.println(new Vec(i2f(t_train.shape())));
        System.out.println(new Vec(i2f(x_test.shape())));
        System.out.println(new Vec(i2f(t_test.shape())));
        maxIndex(t_train);
        printImage(x_train);
        maxIndex(t_test);
        printImage(x_test);

        TwoLayerNet network = new TwoLayerNet(784, 50, 10, 0.01f);

        int iters_num = 10000;
        int train_size = x_train.shape()[0];
        int batch_size = 100;
        float learning_rate = 0.1f;

        ArrayList<Float> train_loss_list = new ArrayList<>();
        ArrayList<Float> train_acc_list = new ArrayList<>();
        ArrayList<Float> test_acc_list = new ArrayList<>();

        float iter_per_epoch = Math.max(train_size / (float) batch_size, 1);

        for (int i = 0; i < iters_num; i++) {
            int[] batch_mask = randomChoice(train_size, batch_size);
            Mat x_batch = x_train.choice(batch_mask);
            Mat t_batch = t_train.choice(batch_mask);

            //LinkedHashMap<String, CubeElement> grad = network.gradient(x_batch, t_batch);
            HashMap<String, CubeElement> grad = network.non_layerGradient(x_batch, t_batch);
            network.parameters.put("W1", ((Mat) network.parameters.get("W1")).sub(((Mat) grad.get("W1")).multi(learning_rate)));
            network.parameters.put("b1", ((Vec) network.parameters.get("b1")).sub(((Vec) grad.get("b1")).multi(learning_rate)));
            network.parameters.put("W2", ((Mat) network.parameters.get("W2")).sub(((Mat) grad.get("W2")).multi(learning_rate)));
            network.parameters.put("b2", ((Vec) network.parameters.get("b2")).sub(((Vec) grad.get("b2")).multi(learning_rate)));

            //float loss = network.loss(x_batch, t_batch);
            float loss = network.non_layersLoss(x_batch, t_batch);
            train_loss_list.add(loss);

            if (i % iter_per_epoch == 0) {
                //float train_acc = network.accuracy(x_train, t_train);
                //float test_acc = network.accuracy(x_test, t_test);
                float train_acc = network.non_layersAccuracy(x_train, t_train);
                float test_acc = network.non_layersAccuracy(x_test, t_test);
                train_acc_list.add(train_acc);
                test_acc_list.add(test_acc);
                System.out.println("train acc , test acc |" + train_acc + "," + test_acc);
            }
        }

    }

    private void printImage(Mat image) {
        for (int i = 0; i < 28; i++) {
            for (int j = 0; j < 28; j++) {
                System.out.print(image.getMat()[0][i * 28 + j] <= 0.5 ? "#" : ".");
            }
            System.out.println();
        }
    }

    private void maxIndex(Mat label) {
        int max = 0;
        for (int i = 0; i < label.getColumn(); i++) {
            if (label.getMat()[0][max] < label.getMat()[0][i]) {
                max = i;
            }
        }
        System.out.println(max);
    }

    private int[] randomChoice(int all, int select) {
        int[] ret = new int[select];
        HashSet<Integer> hSet = new HashSet<>();
        Random rnd = new Random();
        for (int i = 0; i < ret.length; i++) {
            ret[i] = rnd.nextInt(all);
            if (hSet.contains(ret[i])) {
                i--;
            } else {
                hSet.add(ret[i]);
            }
        }
        return ret;
    }

    private float[] i2f(int[] arr) {
        float[] ret = new float[arr.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = (float) arr[e]);
        return ret;
    }

    private float[][] i2f(int[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length]);
        return ret;
    }

}

class TwoLayerNet {

    private int input_size;
    private int hidden_size;
    private int output_size;
    private float weight_init_std;

    public LinkedHashMap<String, CubeElement> parameters;
    public LinkedHashMap<String, Layer> layers;
    public SoftmaxWithLoss lastLayer;

    public TwoLayerNet(int input_size, int hidden_size, int output_size, float weight_init_std) {
        this.input_size = input_size;
        this.hidden_size = hidden_size;
        this.output_size = output_size;
        this.weight_init_std = weight_init_std;
        parameters = new LinkedHashMap<>();
        parameters.put("W1", new Mat(input_size, hidden_size, new Random()).multi(weight_init_std));
        parameters.put("b1", new Vec(hidden_size));
        parameters.put("W2", new Mat(hidden_size, output_size, new Random()).multi(weight_init_std));
        parameters.put("b2", new Vec(output_size));

        layers = new LinkedHashMap<>();
        layers.put("Affine1", new Affine((Mat) parameters.get("W1"), (Vec) parameters.get("b1")));
        layers.put("Relu1", new Relu());
        layers.put("Affine2", new Affine((Mat) parameters.get("W2"), (Vec) parameters.get("b2")));

        lastLayer = new SoftmaxWithLoss();
    }

    public HashMap<String, CubeElement> non_layerGradient(Mat x, Mat t) {
        Mat W1 = (Mat) parameters.get("W1");
        Mat W2 = (Mat) parameters.get("W2");
        Vec b1 = (Vec) parameters.get("b1");
        Vec b2 = (Vec) parameters.get("b2");

        int batch_num = x.shape()[0];

        Mat a1 = x.dot(W1).add(b1);
        Mat z1 = Functions.sigmoid(a1);
        Mat a2 = z1.dot(W2).add(b2);
        Mat y = (Mat) Functions.softmax(a2);

        Mat dy = y.sub(t).div(batch_num);
        HashMap<String, CubeElement> grads = new HashMap<>();
        grads.put("W2", z1.Transform().dot(dy));
        grads.put("b2", dy.sum(0));

        Mat da1 = dy.dot(W2.Transform());
        Mat dz1 = Functions.sigmoid_grad(a1).multi(da1);
        grads.put("W1", x.Transform().dot(dz1));
        grads.put("b1", dz1.sum(0));

        return grads;
    }

    public LinkedHashMap<String, CubeElement> gradient(Mat x, Mat t) {
        //xとtの値を使っていない?

        //forward
        loss(x, t);

        //backword
        Mat dout = lastLayer.backward(1.0F);

        ArrayList<Layer> revLayers = new ArrayList<>();
        for (Map.Entry<String, Layer> m : layers.entrySet()) {
            revLayers.add(m.getValue());
        }
        Collections.reverse(revLayers);
        for (Layer layer : revLayers) {
            dout = layer.backward(dout);
        }
        LinkedHashMap<String, CubeElement> grads = new LinkedHashMap<>();

        grads.put("W1", ((Affine) layers.get("Affine1")).getdW());
        grads.put("b1", ((Affine) layers.get("Affine1")).getdb());
        grads.put("W2", ((Affine) layers.get("Affine2")).getdW());
        grads.put("b2", ((Affine) layers.get("Affine2")).getdb());

        return grads;
    }

    public float loss(Mat x, Mat t) {
        Mat y = predict(x);
        return lastLayer.forward(y, t);
    }

    public float non_layersLoss(Mat x, Mat t) {
        Mat y = non_layersPredict(x);
        return Functions.cross_entropy_error(y, t);
    }

    private Mat predict(Mat x) {
        for (Map.Entry<String, Layer> m : layers.entrySet()) {//layersのlayerは毎回更新されているか
            x = m.getValue().forward(x);
        }
        return x;
    }

    private Mat non_layersPredict(Mat x) {
        Mat W1 = (Mat) parameters.get("W1");
        Mat W2 = (Mat) parameters.get("W2");
        Vec b1 = (Vec) parameters.get("b1");
        Vec b2 = (Vec) parameters.get("b2");
        Mat a1 = x.dot(W1).add(b1);
        Mat z1 = Functions.sigmoid(a1);
        Mat a2 = z1.dot(W2).add(b2);
        Mat y = (Mat) Functions.softmax(a2);
        return y;
    }

    public float accuracy(Mat x, Mat t) {
        Mat y = predict(x);//yが毎回同じ
        int[] yy = y.argmax(1);
        int[] tt = t.argmax(1);
        int count = 0;
        for (int i = 0; i < yy.length; i++) {
            if (yy[i] == tt[i]) {
                count++;
            }
        }
        return count / (float) x.shape()[0];
    }

    public float non_layersAccuracy(Mat x, Mat t) {
        Mat y = non_layersPredict(x);
        int[] yy = y.argmax(1);
        int[] tt = t.argmax(1);
        int count = 0;
        for (int i = 0; i < yy.length; i++) {
            if (yy[i] == tt[i]) {
                count++;
            }
        }
        return count / (float) x.shape()[0];
    }

}

interface Layer {

    Mat forward(Mat x);

    Mat backward(Mat dout);

}

class Relu implements Layer {

    private boolean[][] mask;

    @Override
    public Mat forward(Mat x) {
        this.mask = x.extract(e -> e <= 0);
        Mat out = new Mat(x.mat);
        out = out.mask(this.mask, 0);
        return out;
    }

    @Override
    public Mat backward(Mat dout) {
        dout = dout.mask(this.mask, 0);
        Mat dx = dout;
        return dx;
    }

}

class Affine implements Layer {

    private Mat w;
    private Vec b;
    private Mat dW;
    private Vec db;
    private Mat x;
    private int[] original_x_shape;

    public Affine(Mat w, Vec b) {
        this.w = w;
        this.b = b;
    }

    @Override
    public Mat forward(Mat x) {
        this.original_x_shape = x.shape();
        x = x.reshape(x.shape()[0], -1);
        this.x = x;
        Mat out = this.x.dot(this.w).add(this.b);
        return out;
    }

    @Override
    public Mat backward(Mat dout) {
        Mat dx = dout.dot(this.w.Transform());
        this.dW = this.x.Transform().dot(dout);
        this.db = dout.sum(0);
        dx = dx.reshape(original_x_shape[0],original_x_shape[1]);
        return dx;
    }

    public Mat getdW() {
        return dW;
    }

    public Vec getdb() {
        return db;
    }

}

class SoftmaxWithLoss {

    private float loss;
    private Mat y;
    private Mat t;

    public float forward(Mat x, Mat t) {
        this.t = t;
        this.y = Functions.softmax(x);
        this.loss = Functions.cross_entropy_error(this.y, this.t);
        return this.loss;
    }

    public Mat backward(float dout) {
        int batch_size = this.t.size();
        Mat dx = null;
        if (this.t.size() == this.y.size()) {//one-hot-vector
            dx = this.y.sub(this.t).div(batch_size);
        }
        return dx;
    }

}

class Functions {

    public static Mat sigmoid(Mat x) {
        return new Mat(x.getRow(), x.getColumn(), 1).div(new Mat(x.getRow(), x.getColumn(), 1).add(x.multi(-1).exp()));
    }

    public static Mat softmax(Mat x) {
        x = x.Transform();
        x = x.sub(x.max(0));
        Mat y = x.exp();
        Vec yy = x.exp().sum(0);
        Mat ret = y.div(yy);
        return ret.Transform();
    }

    public static Mat sigmoid_grad(Mat x) {
        return new Mat(x.getRow(), x.getColumn(), 1).sub(sigmoid(x)).multi(sigmoid(x));
    }

    public static float cross_entropy_error(Mat y, Mat t) {
        int batch_size = y.shape()[0];
        int[] maxIndex = t.argmax(1);
        float[] extVec = new float[batch_size];
        IntStream.range(0, batch_size).parallel().forEach(e -> extVec[e] = y.getMat()[e][maxIndex[e]]);
        IntStream.range(0, batch_size).parallel().forEach(e -> extVec[e] = (float) Math.log(extVec[e]));
        float sum = 0;
        for (float f : extVec) {
            sum += f;
        }
        return -sum / batch_size;
    }

}

class CubeElement {

    public int NDIM;

    public CubeElement() {

    }

    public int getNDIM() {
        return NDIM;
    }

}

class Mat extends CubeElement implements Cloneable {

    private int row;
    private int column;
    public float[][] mat;

    public Mat(float[][] mat) {
        this.mat = mat;
        this.row = mat.length;
        this.column = mat[0].length;
        super.NDIM = 2;
    }

    public Mat(int row, int column) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        super.NDIM = 2;
    }

    public Mat(int row, int column, int num) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> {
            mat[e / column][e % column] = num;
        });
        super.NDIM = 2;
    }

    public Mat(int row, int column, Random random) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> {
            mat[e / column][e % column] = (float) gaussianRand(random, 0, 1);
        });
        super.NDIM = 2;
    }

    private double gaussianRand(Random rand, double mean, double stdDev) {
        double u1 = 1.0 - rand.nextDouble();
        double u2 = 1.0 - rand.nextDouble();
        double randStdNormal = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2.0 * Math.PI * u2);
        double randNormal = mean + stdDev * randStdNormal;
        return randNormal;
    }

    public Mat add(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] + target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("ADD RANGE ERROR");
            return null;
        }
    }

    public Mat add(Vec target) {
        return add(target.getMat(row));
    }

    public Mat add(float target) {
        return add(new Vec(column, target));
    }

    public Mat sub(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] - target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("SUB RANGE ERROR");
            return null;
        }
    }

    public Mat sub(Vec target) {
        return sub(target.getMat(row));
    }

    public Mat sub(float target) {
        return sub(new Vec(column, target));
    }

    public Mat multi(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] * target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("MULTI RANGE ERROR");
            return null;
        }
    }

    public Mat multi(Vec target) {
        return multi(target.getMat(row));
    }

    public Mat multi(float target) {
        return multi(new Vec(column, target));
    }

    public Mat div(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] / target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("DIV RANGE ERROR");
            return null;
        }
    }

    public Mat div(Vec target) {
        return div(target.getMat(row));
    }

    public Mat div(float target) {
        return div(new Vec(column, target));
    }

    public Mat dot(Mat target) {
        if (column == target.row) {
            Mat ret = new Mat(row, target.column);
            IntStream.range(0, ret.row * ret.column).parallel().forEach(e -> {
                int x = e % ret.column;
                int y = e / ret.column;
                float sum = 0;
                for (int i = 0; i < column; i++) {
                    sum += mat[y][i] * target.mat[i][x];
                }
                ret.mat[y][x] = sum;
            });
            return ret;
        } else {
            System.out.println("DOT RANGE ERROR");
            return null;
        }
    }

    public Mat exp() {
        Mat ret = new Mat(row, column);
        IntStream.range(0, row * column).parallel().forEach(e -> {
            int x = e % column;
            int y = e / column;
            ret.mat[y][x] = (float) Math.exp(mat[y][x]);
        });
        //return ret.abs();
        return ret;
    }

    public Mat abs() {
        Mat ret = new Mat(row, column);
        IntStream.range(0, row * column).parallel().forEach(e -> {
            int x = e % column;
            int y = e / column;
            ret.mat[y][x] = (float) Math.abs(mat[y][x]);
        });
        return ret;
    }

    private boolean rangeCheck(Mat a, Mat b) {
        return a.row == b.row && a.column == b.column;
    }

    public Mat Transform() {
        Mat ret = new Mat(column, row);
        IntStream.range(0, column * row).parallel().forEach(e -> {
            ret.mat[e % column][e / column] = mat[e / column][e % column];
        });
        return ret;
    }

    public Mat reshape(int[] arr) {
        return reshape(arr[0], arr[1]);
    }

    public Mat reshape(int newRow, int newColumn) {
        if (newColumn == -1) {
            newColumn = this.row * this.column / newRow;
        }
        float[][] ret = new float[newRow][newColumn];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                int x = (i * mat[i].length + j) % newColumn;
                int y = (i * mat[i].length + j) / newColumn;
                ret[y][x] = mat[i][j];
            }
        }
        return new Mat(ret);
    }

    public boolean[][] extract(Function<Float, Boolean> delegate) {
        boolean[][] ret = new boolean[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> ret[e / column][e % column] = delegate.apply(mat[e / column][e % column]));
        return ret;
    }

    //maskがtrueの要素はreplaceの値でreplaceする
    public Mat mask(boolean[][] mask, float replace) {
        if (mask.length == row && mask[0].length == column) {
            float[][] ret = new float[row][column];
            IntStream.range(0, row * column).parallel().forEach(e -> ret[e / column][e % column] = mask[e / column][e % column] ? replace : mat[e / column][e % column]);
            return new Mat(ret);
        } else {
            return null;
        }
    }

    @Override
    public Mat clone() {
        Mat ret = null;
        try {
            ret = (Mat) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < row; i++) {
            sb.append("[");
            for (int j = 0; j < column; j++) {
                sb.append(mat[i][j] + (j < column - 1 ? "," : ""));
            }
            sb.append("]" + (i < row - 1 ? "\n" : ""));
        }
        sb.append("]");
        return sb.toString();
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Vec max(int axis) {
        float[] ret = new float[axis % 2 == 0 ? column : row];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            float max = Float.MIN_VALUE;
            for (int i = 0; i < (axis % 2 == 0 ? row : column); i++) {
                float tmp = (axis % 2 == 0 ? mat[i][e] : mat[e][i]);
                if (max < tmp) {
                    max = tmp;
                }
            }
            ret[e] = max;
        });
        return new Vec(ret);
    }

    public Vec sum(int axis) {
        float[] ret = new float[axis % 2 == 1 ? row : column];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            float sum = 0;
            for (int i = 0; i < (axis % 2 == 1 ? column : row); i++) {
                sum += axis % 2 == 1 ? mat[e][i] : mat[i][e];
            }
            ret[e] = sum;
        });
        return new Vec(ret);
    }

    public int[] argmax(int axis) {
        int[] ret = new int[axis % 2 == 1 ? row : column];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            int max = 0;
            for (int i = 0; i < (axis % 2 == 1 ? column : row); i++) {
                float tmp = axis % 2 == 1 ? mat[e][i] : mat[i][e];
                float maxValue = axis % 2 == 1 ? mat[e][max] : mat[max][e];
                if (maxValue < tmp) {
                    max = i;
                }
            }
            ret[e] = max;
        });
        return ret;
    }

    public Mat choice(int[] indexes) {
        float[][] ret = new float[indexes.length][mat[0].length];
        IntStream.range(0, indexes.length).parallel().forEach(e -> {
            for (int i = 0; i < ret[0].length; i++) {
                ret[e][i] = mat[indexes[e]][i];
            }
        });
        return new Mat(ret);
    }

    public int[] shape() {
        int[] ret = {row, column};
        return ret;
    }

    public int size() {
        return row * column;
    }

    public float[][] getMat() {
        return mat;
    }

}

class Vec extends CubeElement implements Cloneable {

    public float[] vec;

    public Vec(float[] vec) {
        this.vec = vec;
        super.NDIM = 1;
    }

    public Vec(int column) {
        this.vec = new float[column];
    }

    public Vec(int column, float num) {
        this.vec = new float[column];
        IntStream.range(0, vec.length).parallel().forEach(e -> vec[e] = num);
    }

    public Mat getMat(int row) {
        float[][] ret = new float[row][vec.length];
        IntStream.range(0, row * vec.length).parallel().forEach(e -> ret[e / vec.length][e % vec.length] = vec[e % vec.length]);
        return new Mat(ret);
    }

    public Mat add(Mat target) {
        return getMat(target.getRow()).add(target);
    }

    public Vec add(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] + target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec add(float target) {
        return add(new Vec(vec.length, target));
    }

    public Mat sub(Mat target) {
        return getMat(target.getRow()).sub(target);
    }

    public Vec sub(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] - target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec sub(float target) {
        return sub(new Vec(vec.length, target));
    }

    public Mat multi(Mat target) {
        return getMat(target.getRow()).multi(target);
    }

    public Vec multi(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] * target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec multi(float target) {
        return multi(new Vec(vec.length, target));
    }

    public Mat div(Mat target) {
        return getMat(target.getRow()).div(target);
    }

    public Vec div(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] / target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec div(float target) {
        return div(new Vec(vec.length, target));
    }

    public Vec exp() {
        Vec ret = new Vec(vec.length);
        IntStream.range(0, vec.length).parallel().forEach(e -> ret.vec[e] = (float) Math.exp(vec[e]));
        return ret;
    }

    public float max() {
        float n = Float.MIN_VALUE;
        for (float f : vec) {
            if (n < f) {
                n = f;
            }
        }
        return n;
    }

    public float sum() {
        float n = 0;
        for (float f : vec) {
            n += f;
        }
        return n;
    }

    //maskがtrueの要素はreplaceの値でreplaceする
    public Vec mask(boolean[] mask, float replace) {
        if (mask.length == vec.length) {
            float[] ret = new float[mask.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = mask[e] ? replace : vec[e]);
            return new Vec(ret);
        } else {
            return null;
        }
    }

    public boolean[] extract(Function<Float, Boolean> delegate) {
        boolean[] ret = new boolean[vec.length];
        IntStream.range(0, vec.length).parallel().forEach(e -> ret[e] = delegate.apply(vec[e]));
        return ret;
    }

    @Override
    public Vec clone() {
        Vec ret = null;
        try {
            ret = (Vec) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public int size() {
        return vec.length;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < vec.length; i++) {
            sb.append(vec[i] + (i == vec.length - 1 ? "" : ","));
        }
        sb.append("]");
        return sb.toString();
    }

}

class AllDelete {

    public static void deleteDirectory(final String dirPath) throws Exception {
        File file = new File(dirPath);
        recursiveDeleteFile(file);
    }

    /**
     * 対象のファイルオブジェクトの削除を行う.<BR>
     * ディレクトリの場合は再帰処理を行い、削除する。
     *
     * @param file ファイルオブジェクト
     * @throws Exception
     */
    private static void recursiveDeleteFile(final File file) throws Exception {
        // 存在しない場合は処理終了
        if (!file.exists()) {
            return;
        }
        // 対象がディレクトリの場合は再帰処理
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                recursiveDeleteFile(child);
            }
        }
        // 対象がファイルもしくは配下が空のディレクトリの場合は削除する
        file.delete();
    }

}

class MNIST {

    private String root;
    private byte[][] trainImage;
    private byte[] trainLabel;
    private byte[][] testImage;
    private byte[] testLabel;

    public MNIST(String root) {
        this.root = root;
        File rf = new File(root);
        if(!rf.exists()) {
            rf.mkdir();
            new MNISTDownloader(root);
        }
        try {
            trainImage = readImage(root + "/" + "train-images-idx3-ubyte");
            trainLabel = readLabel(root + "/" + "train-labels-idx1-ubyte");
            testImage = readImage(root + "/" + "t10k-images-idx3-ubyte");
            testLabel = readLabel(root + "/" + "t10k-labels-idx1-ubyte");
            //AllDelete.deleteDirectory(root);
            //System.out.println("delete temporary files");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        System.out.println(trainImage.length);
        System.out.println(trainLabel.length);
        System.out.println(testImage.length);
        System.out.println(testLabel.length);
    }

    private float[][] normalize(float[][] mat, float normalize) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length] / normalize;
        });
        return ret;
    }

    private int[] convMat(byte[] vec) {
        int[] ret = new int[vec.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e]);
        return ret;
    }

    private float[][] convMat(byte[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = Byte.toUnsignedInt(mat[e / ret[0].length][e % ret[0].length]);
        });
        return ret;
    }

    private float[][] convMat(int[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length];
        });
        return ret;
    }

    private int[][] labelVec2Mat(int[] rawLabel) {
        int[][] ret = new int[rawLabel.length][10];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e][rawLabel[e]] = 1);
        return ret;
    }

    private int[] byte2int(byte[] vec) {
        int[] ret = new int[vec.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e]);
        return ret;
    }

    private byte[][] readImage(String path) throws IOException {
        //MNISTDownloader.uncompressFile(new File(path + ".gz"));
        int offset = 16;
        int size = 28;
        byte[] vvec = null;
        try {
            vvec = readBin(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //new File(path).delete();
        final byte[] vec = vvec;
        byte[][] ret = new byte[(vec.length - offset) / (size * size)][size * size];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            for (int i = 0; i < size * size; i++) {
                ret[e][i] = vec[offset + size * size * e + i];
            }
        });
        return ret;
    }

    private byte[] readLabel(String path) throws IOException {
        //MNISTDownloader.uncompressFile(new File(path + ".gz"));
        int offset = 8;
        byte[] vvec = null;
        try {
            vvec = readBin(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //new File(path).delete();
        final byte[] vec = vvec;
        byte[] ret = new byte[vec.length - offset];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e + offset]);
        return ret;
    }

    private byte[] readBin(String path) throws IOException {
        return BinaryIO.binaryRead(path);
    }

    public float[][] getTrainImageFloat(float normalized) {
        return normalize(convMat(trainImage), normalized);
    }

    public byte[][] getTrainImageByte() {
        return trainImage;
    }

    public byte[] getTrainLabel() {
        return trainLabel;
    }

    public int[][] getTrainLabelOneHot() {
        return labelVec2Mat(byte2int(trainLabel));
    }

    public float[][] getTrainLabelOneHotFloat() {
        return convMat(labelVec2Mat(byte2int(trainLabel)));
    }

    public float[][] getTestImageFloat(float normalized) {
        return normalize(convMat(testImage), normalized);
    }

    public byte[][] getTestImageByte() {
        return testImage;
    }

    public byte[] getTestLabel() {
        return testLabel;
    }

    public int[][] getTestLabelOneHot() {
        return labelVec2Mat(byte2int(testLabel));
    }

    public float[][] getTestLabelOneHotFloat() {
        return convMat(labelVec2Mat(byte2int(testLabel)));
    }


}

class BinaryIO {

    public static byte[] binaryRead(String path) {
        try {
            byte[] ret = new byte[fileLength(path)];
            byte[] buf = new byte[256];
            FileInputStream input = new FileInputStream(path);
            int len;
            for (int i = 0; (len = input.read(buf)) != -1; i++) {
                for (int j = 0; j < len; j++) {
                    ret[i * buf.length + j] = buf[j];
                }
            }
            input.close();
            return ret;
        } catch (IOException e) {
            return null;
        }
    }

    private static int fileLength(String path) {
        int ret = 0;
        try {
            int len;
            byte[] buf = new byte[256];
            FileInputStream input = new FileInputStream(path);
            while ((len = input.read(buf)) != -1) {
                ret += len;
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private static byte[] vecFix(ArrayList<Byte> vec) {
        byte[] ret = new byte[vec.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = vec.get(i);
        }
        return ret;
    }

    public static void binaryWrite(String path, byte[] bytes) {
        try {
            FileOutputStream output = new FileOutputStream(path);
            output.write(bytes, 0, bytes.length);
            output.flush();
            output.close();
        } catch (IOException e) {

        }
    }
}

class MNISTDownloader {

    protected static final int EOF = -1;
    private String dstDir = "";

    public MNISTDownloader() {
        downloadNIST();
    }

    public MNISTDownloader(String dstDir) {
        this.dstDir = dstDir;
        downloadNIST();
    }

    private void downloadNIST() {
        String[] targets = {"train-images-idx3-ubyte", "train-labels-idx1-ubyte", "t10k-images-idx3-ubyte", "t10k-labels-idx1-ubyte"};
        for (String s : targets) {
            try {
                uncompressFile(downloadWrapper(s));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
    http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
    http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
    http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
    */
    private File downloadWrapper(String fileName) {
        System.out.println("downloading..." + fileName);

        if (download("http://yann.lecun.com/exdb/mnist/" + fileName + ".gz", (dstDir.equals("") ? "" : dstDir + "/") + fileName + ".gz")) {
            return new File((dstDir.equals("") ? "" : dstDir + "/") + fileName + ".gz");
        } else {
            return null;
        }
    }

    private boolean download(String urlStr, String target) {
        if (new File(target).exists()) {
            System.out.println("Skipping...");
            return true;
        } else {
            httpDownloadWrapper(urlStr, target);
            return true;
        }
    }

    public static boolean uncompressFile(File file) throws FileNotFoundException, IOException {
        System.out.println("Extracting...");
        boolean flag = false;
        try {
            BufferedInputStream bis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(file)));
            int n = file.getPath().length();
            String bodyName = file.getPath().substring(0, n - 3);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(bodyName)));

            int c;
            while ((c = bis.read()) != EOF) {
                bos.write((byte) c);
            }
            bis.close();
            bos.close();
            flag = true;
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
        return flag;
    }

    private void httpDownloadWrapper(String src, String dst) {
        try {

            URL url = new URL(src);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setAllowUserInteraction(false);
            conn.setInstanceFollowRedirects(true);
            conn.setRequestMethod("GET");
            conn.connect();

            int httpStatusCode = conn.getResponseCode();

            if (httpStatusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception();
            }

            // Input Stream
            DataInputStream dataInStream
                    = new DataInputStream(
                    conn.getInputStream());

            // Output Stream
            DataOutputStream dataOutStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));

            // Read Data
            byte[] b = new byte[4096];
            int readByte = 0;

            while (-1 != (readByte = dataInStream.read(b))) {
                dataOutStream.write(b, 0, readByte);
            }

            // Close Stream
            dataInStream.close();
            dataOutStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
