import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//円の中心がどこにあるかを判定する必要がある

public class Main {

    public static void main(String[] args){
        new Main().run();
    }

    private void run(){
        Test.main(null);
    }

}

class Test {

    private PointGenerator pg;
    private final int width=900;
    private final int height=900;

    public static void main(String[] args) {
        //new Test().test1();
        //new Test().test2();
        //new Test().test3();
        //new Test().test4();
        new Test().test5();
        //new Test().test6();
    }

    private void test1(){
        pg= new PointGenerator(1024);
        pg.offset(width/2,height/2);
        ImageCanvas ic = new ImageCanvas(width,height);
        ArrayList<Point> points = new ArrayList<>();
        for(Point p:pg.getPoints()){
            points.add(p);
        }
        ArrayList<Line> lines = new ArrayList<>();
        //lines.add(new Line(new Point(100,100),new Point(800,800)));
        ic.addShape(new ShapeData(points,lines));
        new PictureDebug(ic.getCanvas()).run();
    }

    private void test2(){
        double[][] matrixA={
                {2,1,2},
                {1,2,1},
                {2,1,2}
        };
        double[] matrixB = {3,2,1};
        System.out.println(new MatrixCalculator(matrixA, matrixB));
    }

    private void test3(){
        pg= new PointGenerator(1024);
        Circle result = new PointDataCircleAnalysis(convArray(pg.getPoints())).getResult();
        System.out.println(result);
    }

    private ArrayList<Point> convArray(Point[] points){
        ArrayList<Point> ret = new ArrayList<>();
        for(Point p:points){
            ret.add(p);
        }
        return ret;
    }

    private void test4(){
        double[][] testMatrix = {{1,2,0,-1},{-1,1,2,0},{2,0,1,1},{1,-2,-1,1}};
        testMatrix=new InverseMatrixCalculator(testMatrix).getResultMatrix();
        for(int i=0;i<testMatrix.length;i++){
            for(int j=0;j<testMatrix[i].length;j++){
                System.out.print(testMatrix[i][j]+" ");
            }
            System.out.println();
        }
    }

    private void test5(){
        pg=new PointGenerator(1024);
        Circle result = new PointDataCircleAnalysis(convArray(pg.getPoints())).getResult();
        System.out.println(result);
        pg.offset(width / 2, height / 2);
        ArrayList<Line> lines = new ArrayList<>();
        ArrayList<Point> points = convArray(pg.getPoints());
        ImageCanvas ic = new ImageCanvas(width,height);
        ic.addShape(new ShapeData(points,lines));
        new PictureDebug(ic.getCanvas()).run();
    }

    private void test6(){
        Circle circle = new Circle(0,0,200);
        ImageCanvas ic = new ImageCanvas(width,height);
        ArrayList<Point> points = circle.getCirclePoints();
        for(Point p:points){
            p.offset(450,450);
        }
        ArrayList<Line> lines = new ArrayList<>();
        //lines.add(new Line(new Point(100,100),new Point(800,800)));
        ic.addShape(new ShapeData(points,lines));
        new PictureDebug(ic.getCanvas()).run();
    }

}

class CenterSolver {

    private double[] matrixCalculationResult;

    public CenterSolver(double[] matrixCalculationResult){
        this.matrixCalculationResult=matrixCalculationResult;
    }

    public Circle getCircle(){
        if(matrixCalculationResult.length==3){
            return new Circle(-0.5*matrixCalculationResult[0],-0.5*matrixCalculationResult[1],
                    Math.sqrt(-matrixCalculationResult[2]+Math.pow(-0.5*matrixCalculationResult[0],2)+Math.pow(-0.5*matrixCalculationResult[1],2)));
        }else{
            return null;
        }
    }


}

class Circle {

    private double x;
    private double y;
    private double r;

    public Circle(double x,double y,double r){
        this.x=x;
        this.y=y;
        this.r=r;
    }

    public ArrayList<Point> getCirclePoints(){
        ArrayList<Point> ret = new ArrayList<>();
        for(int i=(int)-r;i<r;i++){
            ret.add(new Point(Math.sqrt(r*r-i*i),i));
            ret.add(new Point(-Math.sqrt(r*r-i*i),i));
            ret.add(new Point(i,Math.sqrt(r*r-i*i)));
            ret.add(new Point(i,-Math.sqrt(r*r-i*i)));
        }
        return ret;
    }

    public BufferedImage addCircle(BufferedImage bi){
        for(Point p:getCirclePoints()){
            p.offset(450,450);
            bi.setRGB((int)p.getX(),(int)p.getY(),rgb(255,0,0));
        }
        return bi;
    }

    private int rgb(int r,int g,int b) {
        return 0xff000000 | r << 16 | g << 8 | b;
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public double getR(){
        return r;
    }

    public String toString(){
        return "(x,y)=("+doubleFormatter(x)+","+doubleFormatter(y)+") r="+doubleFormatter(r);
    }

    private String doubleFormatter(double num){
        return String.format("%3f",num);
    }


}

class ImageCanvas {

    private BufferedImage canvas;

    public ImageCanvas(int width,int height){
        canvas = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
    }

    public void addShape(ShapeData sd){
        drawPoint(sd.getPoints());
        for(Line line:sd.getLines()){
            drawPoint(line.getLinePoints());
        }
    }

    private void drawPoint(ArrayList<Point> ps){
       /*
        for(Point p:ps){
            System.out.println(p);
        }
        */
        ps.parallelStream().forEach(e->canvas.setRGB((int) e.getX(), (int) e.getY(), rgb(255, 255, 255)));
    }

    private int rgb(int r,int g,int b) {
        return 0xff000000 | r << 16 | g << 8 | b;
    }

    public BufferedImage getCanvas(){
        return canvas;
    }

}

class InverseMatrixCalculator {

    private double[][] resultMatrix;

    public InverseMatrixCalculator(double[][] matrix) {
        resultMatrix = calculation(matrix);
    }

    private double[][] calculation(double[][] m) {
        double[][] ret = new double[m.length][m[0].length];
        double buf;
        int i, j, k;
        int n = ret.length;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                ret[i][j] = (i == j) ? 1.0 : 0.0;
            }
        }
        for (i = 0; i < n; i++) {
            buf = 1 / m[i][i];
            for (j = 0; j < n; j++) {
                m[i][j] *= buf;
                ret[i][j] *= buf;
            }
            for (j = 0; j < n; j++) {
                if (i != j) {
                    buf = m[j][i];
                    for (k = 0; k < n; k++) {
                        m[j][k] -= m[i][k] * buf;
                        ret[j][k] -= ret[i][k] * buf;
                    }
                }
            }
        }
        return ret;
    }

    public double[][] getResultMatrix(){
        return resultMatrix;
    }

}

class Line {

    private Point start;
    private Point end;

    public Line(Point start,Point end){
        this.start=start;
        this.end=end;
    }

    public void offset(double xOffset,double yOffset){
        start.offset(xOffset, yOffset);
        end.offset(xOffset, yOffset);
    }

    public ArrayList<Point> getLinePoints(){
        ArrayList<Point> ret = new ArrayList<>();
        double diff = (end.getY()-start.getY())/Math.abs(end.getX()-start.getY());
        double y = start.getY();
        for(int i=(int)start.getX();i<=end.getX();i++){
            ret.add(new Point(i,y));
            y+=diff;
        }
        return ret;
    }

}

class MatrixCalculator {

    private double[][] matrixA;
    private double[] matrixB;
    private double[] result;

    public MatrixCalculator(double[][] matrixA,double[] matrixB){
        this.matrixA=new InverseMatrixCalculator(matrixA).getResultMatrix();
        this.matrixB=matrixB;
        result=calculation();
    }

    private double[] calculation(){
        double[] ret = new double[matrixB.length];
        for(int i=0;i<ret.length;i++){
            for(int j=0;j<ret.length;j++){
                ret[i]+=matrixA[i][j]*matrixB[j];
            }
        }
        return ret;
    }

    public double[] getResult(){
        return result;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<result.length;i++){
            for(int j=0;j<matrixA[i].length;j++){
                sb.append(matrixA[i][j]+",");
            }
            sb.append(" "+matrixB[i]+" "+result[i]+"\n");
        }
        return sb.toString();
    }

}


class Phase {

    private double[][] deterArrayA;
    private double[] deterArrayB;

    public Phase(Point firstPoint){
        deterArrayA = new double[3][3];
        deterArrayB = new double[3];
        addPointA(firstPoint);
        addPointB(firstPoint);
    }

    private void addPointA(Point point){
        deterArrayA[0][0]+=Math.pow(point.getX(),2);
        deterArrayA[0][1]+=point.getX()*point.getY();
        deterArrayA[0][2]+=point.getX();
        deterArrayA[1][0]+=point.getX()*point.getY();
        deterArrayA[1][1]+=Math.pow(point.getY(),2);
        deterArrayA[1][2]+=point.getY();
        deterArrayA[2][0]+=point.getX();
        deterArrayA[2][1]+=point.getY();
        deterArrayA[2][2]+=1;
    }

    private void addPointB(Point point){
        deterArrayB[0]+=Math.pow(point.getX(), 3)+point.getX()* Math.pow(point.getY(),2);
        deterArrayB[1]+=Math.pow(point.getX(),2)*point.getY()+Math.pow(point.getY(),3);
        deterArrayB[2]+=Math.pow(point.getX(),2)+Math.pow(point.getY(),2);
    }

    public void reflectionNextPoint(Point point){
        addPointA(point);
        addPointB(point);
    }

    public double[][] getDeterArrayA(){
        return deterArrayA;
    }

    public double[] getDeterArrayB(){
        for(int i=0;i<deterArrayB.length;i++){
            deterArrayB[i]*=-1;
        }
        return deterArrayB;
    }

}


class Point {

    private double x;
    private double y;

    public Point(double x,double y){
        this.x=x;
        this.y=y;
    }

    public void offset(double xOffset,double yOffset){
        x+=xOffset;
        y+=yOffset;
    }

    public double getX(){
        return x;
    }

    public double getY() {
        return y;
    }

    public String toString(){
        return "x:"+x+" y:"+y;
    }

}

class PointDataCircleAnalysis {

    private Phase phase;
    private Circle result;

    public PointDataCircleAnalysis(ArrayList<Point> points){
        result=calculation(makeCalculationData(points));
    }

    private Phase makeCalculationData(ArrayList<Point> points){
        Phase ret = new Phase(points.get(0));
        for(int i=1;i<points.size();i++){
            ret.reflectionNextPoint(points.get(i));
        }
        return ret;
    }

    private Circle calculation(Phase phase){
        //System.out.println(new MatrixCalculator(phase.getDeterArrayA(),phase.getDeterArrayB()));
        return new CenterSolver(new MatrixCalculator(phase.getDeterArrayA(),phase.getDeterArrayB()).getResult()).getCircle();
    }

    public Circle getResult(){
        return result;
    }


}

class PointGenerator {

    private Point[] points;
    private Point center;
    private double r;
    private double scatter;

    public PointGenerator(int pointNumber) {
        points = new Point[pointNumber];
        center = new Point(0.0,0.0);
        r = 200;
        scatter = 30.0;
        calculation();
    }

    public PointGenerator(int pointNumber,Point center,double r,double scatter) {
        points = new Point[pointNumber];
        this.center=center;
        this.r=r;
        this.scatter=scatter;
        calculation();
    }

    private void calculation(){
        IntStream.range(0,points.length).parallel().forEach(e -> points[e] = pointGeneration());
    }

    private Point pointGeneration(){
        double length = calcLength(r,scatter);
        double angle = genAngle();
        Point ret = new Point(length*Math.cos(angle),length*Math.sin(angle));
        return ret;
    }

    private double calcLength(double baseLength,double scatterRange){
        return baseLength-scatterRange/2+scatterRange* new Random().nextDouble();
    }

    private double genAngle(){
        return (double)(new Random().nextInt(360))*Math.PI/180;
    }

    public void offset(double xOffset,double yOffset){
        Stream.of(points).parallel().forEach(e->e.offset(xOffset, yOffset));
    }

    public Point[] getPoints(){
        return points;
    }

}


class ShapeData {

    private ArrayList<Point> points;
    private ArrayList<Line> lines;

    public ShapeData(ArrayList<Point> points,ArrayList<Line> lines){
        this.points=points;
        this.lines=lines;
    }

    public ArrayList<Point> getPoints(){
        return points;
    }

    public ArrayList<Line> getLines(){
        return lines;
    }

    public void offset(double xOffset,double yOffset){
        points.stream().parallel().forEach(e->e.offset(xOffset,yOffset));
    }

}


class PictureDebug extends JFrame{

    private BufferedImage target;

    public PictureDebug(BufferedImage target) {
        this.target = target;
    }

    public void run() {
        JFrame frame = new JFrame("Image");

        JLabel base = new JLabel();
        base.setIcon(new ImageIcon(target));
        JScrollPane scrollPane = new JScrollPane(base);

        frame.getContentPane().add(scrollPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}


