import java.awt.*;
import java.awt.image.AreaAveragingScaleFilter;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.IntStream;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;
import java.io.*;
import java.util.function.Function;
import java.util.zip.GZIPInputStream;

//下記のJARに依存する
//http://ftp.jaist.ac.jp/pub/apache//commons/logging/binaries/commons-logging-1.2-bin.zip
//http://ftp.meisei-u.ac.jp/mirror/apache/dist/pdfbox/2.0.15/pdfbox-2.0.15.jar

//データセットは下記のものを使用
//https://docs.etrobo.jp/rules/BonusNumber_1.0.0.pdf

public class Main {

    public static void main(String[] args) {
        new Main().run4();
    }

    private void run4(){
        ETRoboNumImage dataset = new ETRoboNumImage("BonusNumber_1.0.0.pdf");
        Mat x_train = new Mat(dataset.getTrainImageFloat(255));
        Mat t_train = new Mat(dataset.getTrainLabelOneHotFloat());
        Mat x_test = new Mat(dataset.getTestImageFloat(255));
        Mat t_test = new Mat(dataset.getTestLabelOneHotFloat());

        //Mat valid = makeValid(dataset.getRotatedImages()[0][1000]);//1
        //Mat valid = makeValid(dataset.getRotatedImages()[1][2000]);//2
        //Mat valid = makeValid(dataset.getRotatedImages()[2][3000]);//3
        //Mat valid = makeValid(dataset.getRotatedImages()[3][2000]);//4
        //Mat valid = makeValid(dataset.getRotatedImages()[4][1000]);//5
        //Mat valid = makeValid(dataset.getRotatedImages()[5][2000]);//6
        //Mat valid = makeValid(dataset.getRotatedImages()[6][3000]);//7
        Mat valid = makeValid(dataset.getRotatedImages()[7][2000]);//8
        //Mat valid = new Mat(new TestingImage("dataset").getImageFloat(255));
        debug(valid);

        System.out.println(new Vec(Util.i2f(x_train.shape())));
        System.out.println(new Vec(Util.i2f(t_train.shape())));
        System.out.println(new Vec(Util.i2f(x_test.shape())));
        System.out.println(new Vec(Util.i2f(t_test.shape())));
        Util.maxIndex(t_train);
        Util.printImage(x_train);
        Util.maxIndex(t_test);
        Util.printImage(x_test);

        TwoLayerNet network = new TwoLayerNet(784, 50, dataset.getTypeNum(), 0.01f);

        //int iters_num = 10000;
        int iters_num = 400;
        int train_size = x_train.shape()[0];
        int batch_size = 100;
        float learning_rate = 0.1f;

        ArrayList<Float> train_loss_list = new ArrayList<>();
        ArrayList<Float> train_acc_list = new ArrayList<>();
        ArrayList<Float> test_acc_list = new ArrayList<>();

        //float iter_per_epoch = Math.max(train_size / (float) batch_size, 1);
        float iter_per_epoch = Math.max(10, 1);

        for (int i = 0; i < iters_num; i++) {
            int[] batch_mask = Util.randomChoice(train_size, batch_size);
            Mat x_batch = x_train.choice(batch_mask);
            Mat t_batch = t_train.choice(batch_mask);

            //LinkedHashMap<String, CubeElement> grad = network.gradient(x_batch, t_batch);
            HashMap<String, CubeElement> grad = network.non_layerGradient(x_batch, t_batch);
            network.parameters.put("W1", ((Mat) network.parameters.get("W1")).sub(((Mat) grad.get("W1")).multi(learning_rate)));
            network.parameters.put("b1", ((Vec) network.parameters.get("b1")).sub(((Vec) grad.get("b1")).multi(learning_rate)));
            network.parameters.put("W2", ((Mat) network.parameters.get("W2")).sub(((Mat) grad.get("W2")).multi(learning_rate)));
            network.parameters.put("b2", ((Vec) network.parameters.get("b2")).sub(((Vec) grad.get("b2")).multi(learning_rate)));

            //float loss = network.loss(x_batch, t_batch);
            float loss = network.non_layersLoss(x_batch, t_batch);
            train_loss_list.add(loss);

            if (i % iter_per_epoch == 0) {
                //float train_acc = network.accuracy(x_train, t_train);
                //float test_acc = network.accuracy(x_test, t_test);
                float train_acc = network.non_layersAccuracy(x_train, t_train);
                float test_acc = network.non_layersAccuracy(x_test, t_test);
                train_acc_list.add(train_acc);
                test_acc_list.add(test_acc);
                System.out.println("iters="+i+":train acc , test acc |" + train_acc + "," + test_acc);
            }
        }
        int[] result = network.validation(valid);
        for(int n:result){
            System.out.println(n+1);
        }
    }

    private void run3() {
        MNIST mnist = new MNIST("mnist");
        Mat x_train = new Mat(mnist.getTrainImageFloat(255));
        Mat t_train = new Mat(mnist.getTrainLabelOneHotFloat());
        Mat x_test = new Mat(mnist.getTestImageFloat(255));
        Mat t_test = new Mat(mnist.getTestLabelOneHotFloat());
        System.out.println(new Vec(Util.i2f(x_train.shape())));
        System.out.println(new Vec(Util.i2f(t_train.shape())));
        System.out.println(new Vec(Util.i2f(x_test.shape())));
        System.out.println(new Vec(Util.i2f(t_test.shape())));
        Util.maxIndex(t_train);
        Util.printImage(x_train);
        Util.maxIndex(t_test);
        Util.printImage(x_test);

        TwoLayerNet network = new TwoLayerNet(784, 50, 10, 0.01f);

        int iters_num = 10000;
        int train_size = x_train.shape()[0];
        int batch_size = 100;
        float learning_rate = 0.1f;

        ArrayList<Float> train_loss_list = new ArrayList<>();
        ArrayList<Float> train_acc_list = new ArrayList<>();
        ArrayList<Float> test_acc_list = new ArrayList<>();

        float iter_per_epoch = Math.max(train_size / (float) batch_size, 1);

        for (int i = 0; i < iters_num; i++) {
            int[] batch_mask = Util.randomChoice(train_size, batch_size);
            Mat x_batch = x_train.choice(batch_mask);
            Mat t_batch = t_train.choice(batch_mask);

            //LinkedHashMap<String, CubeElement> grad = network.gradient(x_batch, t_batch);
            HashMap<String, CubeElement> grad = network.non_layerGradient(x_batch, t_batch);
            network.parameters.put("W1", ((Mat) network.parameters.get("W1")).sub(((Mat) grad.get("W1")).multi(learning_rate)));
            network.parameters.put("b1", ((Vec) network.parameters.get("b1")).sub(((Vec) grad.get("b1")).multi(learning_rate)));
            network.parameters.put("W2", ((Mat) network.parameters.get("W2")).sub(((Mat) grad.get("W2")).multi(learning_rate)));
            network.parameters.put("b2", ((Vec) network.parameters.get("b2")).sub(((Vec) grad.get("b2")).multi(learning_rate)));

            //float loss = network.loss(x_batch, t_batch);
            float loss = network.non_layersLoss(x_batch, t_batch);
            train_loss_list.add(loss);

            if (i % iter_per_epoch == 0) {
                //float train_acc = network.accuracy(x_train, t_train);
                //float test_acc = network.accuracy(x_test, t_test);
                float train_acc = network.non_layersAccuracy(x_train, t_train);
                float test_acc = network.non_layersAccuracy(x_test, t_test);
                train_acc_list.add(train_acc);
                test_acc_list.add(test_acc);
                System.out.println("train acc , test acc |" + train_acc + "," + test_acc);
            }
        }

    }

    //train
    private void run2() {
        ArrayList<BufferedImage> images = new PdfToImage().pdfToImage("BonusNumber_1.0.0.pdf");
        BufferedImage[] fixedImages = Util.fixVec(images);
        /*
        for(int i=0;i<6;i++) {
            IntStream.range(0, fixedImages.length).parallel().forEach(j -> fixedImages[j] = Util.multilook(fixedImages[j]));
        }
        */
        IntStream.range(0, fixedImages.length).parallel().forEach(j -> fixedImages[j] = Util.biReSize(fixedImages[j],28,28));
        BufferedImage[][] rotatedImages = new BufferedImage[fixedImages.length][360];
        IntStream.range(0,360*fixedImages.length).parallel().forEach(e->{
            int i=e/fixedImages.length;
            int j=e%fixedImages.length;
            rotatedImages[j][i] = Util.imageRotate(fixedImages[j], i);
        });
        if (new File("result").exists()) {
            try {
                DirUtil.deleteDirectory("result");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        new File("result").mkdir();
        for (int i = 0; i < rotatedImages.length; i++) {
            for(int j=0;j<rotatedImages[i].length;j++) {
                Util.writeImageWrapper(rotatedImages[i][j], "result/" + (i*rotatedImages[i].length+j) + ".png");
            }
        }
    }

    //makeDataset
    private void run() {
        ArrayList<BufferedImage> images = new PdfToImage().pdfToImage("BonusNumber_1.0.0.pdf");
        BufferedImage[] fixedImages = Util.fixVec(images);
        for(int i=0;i<4;i++) {
            IntStream.range(0, fixedImages.length).parallel().forEach(j -> fixedImages[j] = Util.multilook(fixedImages[j]));
        }
        if (new File("result").exists()) {
            try {
                DirUtil.deleteDirectory("result");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        new File("result").mkdir();

        for(int jj=0;jj<fixedImages.length;jj++){
            final int j=jj;
            BufferedImage[] rotatedImages = new BufferedImage[360];
            IntStream.range(0,rotatedImages.length).parallel().forEach(i->rotatedImages[i] = Util.imageRotate(fixedImages[j], i));
            for(int i=0;i<rotatedImages.length;i++) {
                Util.writeImageWrapper(rotatedImages[i], "result/" + (j*rotatedImages.length+i) + ".png");
            }
        }
    }

    private void debug(Mat mat){
        float[][] m = mat.getMat();
        for(int i=0;i<m.length;i++){
            float[][] image = new float[28][28];
            for(int j=0;j<image.length;j++){
                for(int k=0;k<image[j].length;k++){
                    //image[j][k]=m[i][j*28+k]*255;
                    System.out.print(0.5<=m[i][j*28+k]?".":"#");
                }
                System.out.println();
            }
            /*
            BufferedImage im = new BufferedImage(28,28,BufferedImage.TYPE_INT_RGB);
            for(int j=0;j<im.getHeight();j++){
                for(int k=0;k<im.getWidth();k++){
                    im.setRGB(k,j,Util.rgb((int)image[j][k],(int)image[j][k],(int)image[j][k]));
                }
            }
            Util.writeImageWrapper(im,"debug"+i+".png");
            */
        }
    }

    private Mat makeValid(BufferedImage target){
        float[][] ret = new float[1][28*28];
        for(int i=0;i<target.getHeight();i++){
            for(int j=0;j<target.getWidth();j++){
                ret[0][i*28+j]=Util.r(target.getRGB(j,i))/255.0f;
            }
        }
        return new Mat(ret);
    }



}

class TestingImage{

    private BufferedImage[] rotatedImages;

    public TestingImage(String path){
        File[] files = new File(path).listFiles();
        ArrayList<File> filteredFiles = new ArrayList<>();
        for(File f:files){
            if(f.getName().contains(".jpg")){
                filteredFiles.add(f);
            }
        }
        rotatedImages=new BufferedImage[filteredFiles.size()];
        for(int i=0;i<rotatedImages.length;i++){
            rotatedImages[i] = readImageWrapper(filteredFiles.get(i).getAbsolutePath());
        }
        IntStream.range(0, rotatedImages.length).parallel().forEach(j -> rotatedImages[j] = Util.biReSize(rotatedImages[j],28,28));
    }

    private BufferedImage readImageWrapper(String path){
        BufferedImage ret = null;
        try{
            ret = ImageIO.read(new File(path));
        }catch(IOException e){
            e.printStackTrace();
        }
        return ret;
    }

    public float[][] getImageFloat(float normalized){
        ArrayList<Float[]> ret = new ArrayList<>();
        for(int i=0;i<rotatedImages.length;i++){
            ret.add(imagewVec(rotatedImages[i],normalized));
        }
        return fixMat(ret);
    }

    private Float[] imagewVec(BufferedImage target,float normalized){
        Float[] ret = new Float[target.getWidth()*target.getHeight()];
        for(int i=0;i<ret.length;i++){
            ret[i]=((float)Util.r(target.getRGB(i%target.getWidth(),i/target.getWidth())))/normalized;
        }
        return ret;
    }

    private float[][] fixMat(ArrayList<Float[]> mat){
        float[][] ret = new float[mat.size()][];
        for(int i=0;i<ret.length;i++){
            ret[i]=convVec(mat.get(i));
        }
        return ret;
    }

    private float[] convVec(Float[] vec){
        float[] ret = new float[vec.length];
        for(int i=0;i<ret.length;i++){
            ret[i]=vec[i];
        }
        return ret;
    }

}

class ETRoboNumImage{

    private String pdfPath;
    private BufferedImage[][] rotatedImages;

    private static final int split = 10;//1度を何分割してデータセットを作るか

    public ETRoboNumImage(String pdfPath){
        this.pdfPath=pdfPath;
        ArrayList<BufferedImage> images = new PdfToImage().pdfToImage(pdfPath);
        BufferedImage[] fixedImages = Util.fixVec(images);
        IntStream.range(0, fixedImages.length).parallel().forEach(j -> fixedImages[j] = Util.biReSize(fixedImages[j],28,28));
        rotatedImages = new BufferedImage[fixedImages.length][360*split];
        IntStream.range(0,360*split*fixedImages.length).parallel().forEach(e->{
            int i=e/fixedImages.length;
            int j=e%fixedImages.length;
            rotatedImages[j][i] = Util.imageRotate(fixedImages[j], i/((float)split));
        });
    }

    //trainには10で割り切れる画像をチョイスする
    //testには10で割り切れない画像をチョイスする

    public float[][] getTrainImageFloat(float normalized){
        ArrayList<Float[]> ret = new ArrayList<>();
        for(int i=0;i<rotatedImages.length;i++){
            for(int j=0;j<rotatedImages[i].length;j++){
                if(j%10==0){
                    ret.add(imagewVec(rotatedImages[i][j],normalized));
                }
            }
        }
        return fixMat(ret);
    }

    public float[][] getTrainLabelOneHotFloat(){
        ArrayList<Float[]> ret = new ArrayList<>();
        for(int i=0;i<rotatedImages.length;i++){
            for(int j=0;j<rotatedImages[i].length;j++){
                if(j%10==0){
                    ret.add(makeOnehot(i,rotatedImages.length));
                }
            }
        }
        return fixMat(ret);
    }

    public float[][] getTestImageFloat(float normalized){
        ArrayList<Float[]> ret = new ArrayList<>();
        for(int i=0;i<rotatedImages.length;i++){
            for(int j=0;j<rotatedImages[i].length;j++){
                if(j%10!=0){
                    ret.add(imagewVec(rotatedImages[i][j],normalized));
                }
            }
        }
        return fixMat(ret);
    }

    public float[][] getTestLabelOneHotFloat(){
        ArrayList<Float[]> ret = new ArrayList<>();
        for(int i=0;i<rotatedImages.length;i++){
            for(int j=0;j<rotatedImages[i].length;j++){
                if(j%10!=0){
                    ret.add(makeOnehot(i,rotatedImages.length));
                }
            }
        }
        return fixMat(ret);
    }

    private Float[] imagewVec(BufferedImage target,float normalized){
        Float[] ret = new Float[target.getWidth()*target.getHeight()];
        for(int i=0;i<ret.length;i++){
            ret[i]=((float)Util.r(target.getRGB(i%target.getWidth(),i/target.getWidth())))/normalized;
        }
        return ret;
    }

    private Float[] makeOnehot(int target,int length){
        Float[] ret = new Float[length];
        for(int i=0;i<ret.length;i++){
            ret[i]=0.0f;
        }
        ret[target]=1.0f;
        return ret;
    }

    private float[][] fixMat(ArrayList<Float[]> mat){
        float[][] ret = new float[mat.size()][];
        for(int i=0;i<ret.length;i++){
            ret[i]=convVec(mat.get(i));
        }
        return ret;
    }

    private float[] convVec(Float[] vec){
        float[] ret = new float[vec.length];
        for(int i=0;i<ret.length;i++){
            ret[i]=vec[i];
        }
        return ret;
    }

    public int getTypeNum(){
        return rotatedImages.length;
    }

    public BufferedImage[][] getRotatedImages() {
        return rotatedImages;
    }
}

class Util{

    public static BufferedImage biReSize(BufferedImage image, int width, int height) {
        BufferedImage thumb = new BufferedImage(width, height, image.getType());
        thumb.getGraphics().drawImage(image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING), 0, 0, width, height, null);
        return thumb;
    }

    public static void printImage(Mat image) {
        for (int i = 0; i < 28; i++) {
            for (int j = 0; j < 28; j++) {
                System.out.print(image.getMat()[0][i * 28 + j] <= 0.5 ? "#" : ".");
            }
            System.out.println();
        }
    }

    public static void maxIndex(Mat label) {
        int max = 0;
        for (int i = 0; i < label.getColumn(); i++) {
            if (label.getMat()[0][max] < label.getMat()[0][i]) {
                max = i;
            }
        }
        System.out.println(max);
    }

    public static int[] randomChoice(int all, int select) {
        int[] ret = new int[select];
        HashSet<Integer> hSet = new HashSet<>();
        Random rnd = new Random();
        for (int i = 0; i < ret.length; i++) {
            ret[i] = rnd.nextInt(all);
            if (hSet.contains(ret[i])) {
                i--;
            } else {
                hSet.add(ret[i]);
            }
        }
        return ret;
    }

    public static float[] i2f(int[] arr) {
        float[] ret = new float[arr.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = (float) arr[e]);
        return ret;
    }

    public static float[][] i2f(int[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length]);
        return ret;
    }

    public static BufferedImage imageRotate(BufferedImage src, double deg) {
        return new ImageRotator(src, deg).getResult();
    }

    public static void writeImageWrapper(BufferedImage target, String path) {
        try {
            ImageIO.write(target, "png", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BufferedImage multilook(BufferedImage src) {
        BufferedImage ret = new BufferedImage(src.getWidth() / 2, src.getHeight() / 2, BufferedImage.TYPE_INT_RGB);
        IntStream.range(0, ret.getWidth() * ret.getHeight()).forEach(e -> {
            int x = e % ret.getWidth();
            int y = e / ret.getWidth();
            int r = 0;
            int g = 0;
            int b = 0;
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    r += r(src.getRGB(x * 2 + j, y * 2 + i));
                    g += g(src.getRGB(x * 2 + j, y * 2 + i));
                    b += b(src.getRGB(x * 2 + j, y * 2 + i));
                }
            }
            ret.setRGB(x, y, rgb(r / 4, g / 4, b / 4));
        });
        return ret;
    }

    public static int r(int c) {
        return c >> 16 & 0xff;
    }

    public static int g(int c) {
        return c >> 8 & 0xff;
    }

    public static int b(int c) {
        return c & 0xff;
    }

    public static int rgb(int r, int g, int b) {
        return 0xff000000 | r << 16 | g << 8 | b;
    }

    public static BufferedImage[] fixVec(ArrayList<BufferedImage> src) {
        BufferedImage[] ret = new BufferedImage[src.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = src.get(i);
        }
        return ret;
    }

}

class ImageRotator {

    private BufferedImage src;
    private BufferedImage dst;
    private double angle;

    public ImageRotator(BufferedImage src, double angle) {
        this.src = src;
        this.angle = angle;
        this.dst = calculation(src.getWidth(), src.getHeight());
    }

    private BufferedImage calculation(int width, int height) {
        double rd = angle * Math.PI / 180.0;
        BufferedImage dst = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        IntStream.range(0, width * height).forEach(e -> {
            int ix = e % width;
            int iy = e / width;
            double x = ix - width / 2;
            double y = iy - height / 2;

            int sx = (int) (x * Math.cos(rd) - y * Math.sin(rd) + src.getWidth() / 2);
            int sy = (int) (x * Math.sin(rd) + y * Math.cos(rd) + src.getHeight() / 2);


            if (0 <= sx && 0 <= sy && sx < src.getWidth() && sy < src.getHeight()) {
                int psrc = src.getRGB(sx, sy);
                dst.setRGB(ix, iy, psrc);
            } else {
                dst.setRGB(ix, iy, 0x00FFFFFF);
            }
        });
        return dst;
    }

    public BufferedImage getResult() {
        return dst;
    }

}

class PdfToImage {

    // サムネイル画像の縦横比率
    private static final double RETIO = 0.75;
    // サムネイル画像の幅(400px)
    private static final double IMAGE_WIDTH = 1000;

    /**
     * PDFファイルを画像化する
     *
     * @param pdfPath PDFへのパス
     * @return
     */
    public void pdfToImageWithSave(String pdfPath) {

        // PDFドキュメントをロード
        try (PDDocument document = PDDocument.load(new File(pdfPath));) {

            PDFRenderer pdfRenderer = new PDFRenderer(document);

            for (int page = 0; page < document.getNumberOfPages(); ++page) {
                // ページごとの情報を取得する。
                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

                //BufferedImage resizedBim = scaleImage(bim);
                BufferedImage resizedBim = bim;

                FileOutputStream fos = new FileOutputStream(page + ".png");

                ImageOutputStream ios = ImageIO.createImageOutputStream(fos);
                ImageIO.write(resizedBim, "PNG", ios);

            }
        } catch (IOException e) {
            // エラー時処理
        }
    }

    public ArrayList<BufferedImage> pdfToImage(String pdfPath) {

        ArrayList<BufferedImage> ret = new ArrayList<>();
        // PDFドキュメントをロード
        try (PDDocument document = PDDocument.load(new File(pdfPath));) {

            PDFRenderer pdfRenderer = new PDFRenderer(document);

            for (int page = 0; page < document.getNumberOfPages(); ++page) {
                // ページごとの情報を取得する。
                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

                ret.add(bim);

            }
        } catch (IOException e) {
            // エラー時処理
        }
        return ret;
    }

    /**
     * 縦横比を維持したまま画像サイズを変更し、上下または左右に余白をつける
     *
     * @param in 元画像データ
     * @return リサイズした画像データ
     */
    public BufferedImage scaleImage(BufferedImage in) {
        double scale = getScale((double) in.getWidth(), (double) in.getHeight());
        int scaledWidth = (int) (in.getWidth() * scale + 0.5);
        int scaledHeight = (int) (in.getHeight() * scale + 0.5);

        // リサイズ
        // 領域平均化アルゴリズムを利用
        // https://docs.oracle.com/javase/jp/1.3/api/java/awt/image/AreaAveragingScaleFilter.html
        ImageFilter filter = new AreaAveragingScaleFilter(scaledWidth, scaledHeight);
        ImageProducer imageProducer = new FilteredImageSource(in.getSource(), filter);
        java.awt.Image dstImage = Toolkit.getDefaultToolkit().createImage(imageProducer);

        // 画像ファイル出力時に使うにBufferを作成。画像は縦横比がRETIOと一致するようにする
        BufferedImage dst = new BufferedImage((int) IMAGE_WIDTH, (int) (IMAGE_WIDTH * RETIO),
                BufferedImage.TYPE_INT_RGB);

        // 背景色のみの画像データを作成する
        Graphics2D g = dst.createGraphics();
        // 背景色を白に塗り替える
        g.setBackground(Color.WHITE);
        g.clearRect(0, 0, (int) IMAGE_WIDTH, (int) (IMAGE_WIDTH * RETIO));

        // 画像の左上の位置を指定して中央に配置し、余白をつける。
        // x座標の位置（左右の位置）
        int x = ((int) IMAGE_WIDTH - scaledWidth) / 2;
        // y座標の位置（上下の位置）
        int y = ((int) (IMAGE_WIDTH * RETIO) - scaledHeight) / 2;
        // リサイズした画像を反映させる。
        g.drawImage(dstImage, x, y, null);
        g.dispose();
        return dst;
    }

    /**
     * 画像の拡大率を取得する
     *
     * @param width  元画像の横幅
     * @param height 元画像の縦幅
     * @return 拡大率
     */
    private double getScale(double width, double height) {
        double imageRetio = height / width;
        double scale = 0;

        if (imageRetio <= RETIO) {
            // 指定比率よりも横幅が大きい時、拡大率は幅に合わせる
            scale = IMAGE_WIDTH / width;
        } else {
            // 指定比率よりも横幅が小さい時、拡大率は高さに合わせる
            scale = IMAGE_WIDTH * RETIO / height;
        }
        return scale;
    }
}

class DirUtil {

    /**
     * 対象パスのディレクトリの削除を行う.<BR>
     * ディレクトリ配下のファイル等が存在する場合は<BR>
     * 配下のファイルをすべて削除します.
     *
     * @param dirPath 削除対象ディレクトリパス
     * @throws Exception
     */
    public static void deleteDirectory(final String dirPath) throws Exception {
        File file = new File(dirPath);
        recursiveDeleteFile(file);
    }

    /**
     * 対象のファイルオブジェクトの削除を行う.<BR>
     * ディレクトリの場合は再帰処理を行い、削除する。
     *
     * @param file ファイルオブジェクト
     * @throws Exception
     */
    private static void recursiveDeleteFile(final File file) throws Exception {
        // 存在しない場合は処理終了
        if (!file.exists()) {
            return;
        }
        // 対象がディレクトリの場合は再帰処理
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                recursiveDeleteFile(child);
            }
        }
        // 対象がファイルもしくは配下が空のディレクトリの場合は削除する
        file.delete();
    }

}

class TwoLayerNet {

    private int input_size;
    private int hidden_size;
    private int output_size;
    private float weight_init_std;

    public LinkedHashMap<String, CubeElement> parameters;
    public LinkedHashMap<String, Layer> layers;
    public SoftmaxWithLoss lastLayer;

    public TwoLayerNet(int input_size, int hidden_size, int output_size, float weight_init_std) {
        this.input_size = input_size;
        this.hidden_size = hidden_size;
        this.output_size = output_size;
        this.weight_init_std = weight_init_std;
        parameters = new LinkedHashMap<>();
        parameters.put("W1", new Mat(input_size, hidden_size, new Random()).multi(weight_init_std));
        parameters.put("b1", new Vec(hidden_size));
        parameters.put("W2", new Mat(hidden_size, output_size, new Random()).multi(weight_init_std));
        parameters.put("b2", new Vec(output_size));

        layers = new LinkedHashMap<>();
        layers.put("Affine1", new Affine((Mat) parameters.get("W1"), (Vec) parameters.get("b1")));
        layers.put("Relu1", new Relu());
        layers.put("Affine2", new Affine((Mat) parameters.get("W2"), (Vec) parameters.get("b2")));

        lastLayer = new SoftmaxWithLoss();
    }

    public HashMap<String, CubeElement> non_layerGradient(Mat x, Mat t) {
        Mat W1 = (Mat) parameters.get("W1");
        Mat W2 = (Mat) parameters.get("W2");
        Vec b1 = (Vec) parameters.get("b1");
        Vec b2 = (Vec) parameters.get("b2");

        int batch_num = x.shape()[0];

        Mat a1 = x.dot(W1).add(b1);
        Mat z1 = Functions.sigmoid(a1);
        Mat a2 = z1.dot(W2).add(b2);
        Mat y = (Mat) Functions.softmax(a2);

        Mat dy = y.sub(t).div(batch_num);
        HashMap<String, CubeElement> grads = new HashMap<>();
        grads.put("W2", z1.Transform().dot(dy));
        grads.put("b2", dy.sum(0));

        Mat da1 = dy.dot(W2.Transform());
        Mat dz1 = Functions.sigmoid_grad(a1).multi(da1);
        grads.put("W1", x.Transform().dot(dz1));
        grads.put("b1", dz1.sum(0));

        return grads;
    }

    public LinkedHashMap<String, CubeElement> gradient(Mat x, Mat t) {
        //xとtの値を使っていない?

        //forward
        loss(x, t);

        //backword
        Mat dout = lastLayer.backward(1.0F);

        ArrayList<Layer> revLayers = new ArrayList<>();
        for (Map.Entry<String, Layer> m : layers.entrySet()) {
            revLayers.add(m.getValue());
        }
        Collections.reverse(revLayers);
        for (Layer layer : revLayers) {
            dout = layer.backward(dout);
        }
        LinkedHashMap<String, CubeElement> grads = new LinkedHashMap<>();

        grads.put("W1", ((Affine) layers.get("Affine1")).getdW());
        grads.put("b1", ((Affine) layers.get("Affine1")).getdb());
        grads.put("W2", ((Affine) layers.get("Affine2")).getdW());
        grads.put("b2", ((Affine) layers.get("Affine2")).getdb());

        return grads;
    }

    public float loss(Mat x, Mat t) {
        Mat y = predict(x);
        return lastLayer.forward(y, t);
    }

    public float non_layersLoss(Mat x, Mat t) {
        Mat y = non_layersPredict(x);
        return Functions.cross_entropy_error(y, t);
    }

    private Mat predict(Mat x) {
        for (Map.Entry<String, Layer> m : layers.entrySet()) {//layersのlayerは毎回更新されているか
            x = m.getValue().forward(x);
        }
        return x;
    }

    private Mat non_layersPredict(Mat x) {
        Mat W1 = (Mat) parameters.get("W1");
        Mat W2 = (Mat) parameters.get("W2");
        Vec b1 = (Vec) parameters.get("b1");
        Vec b2 = (Vec) parameters.get("b2");
        Mat a1 = x.dot(W1).add(b1);
        Mat z1 = Functions.sigmoid(a1);
        Mat a2 = z1.dot(W2).add(b2);
        Mat y = (Mat) Functions.softmax(a2);
        return y;
    }

    public float accuracy(Mat x, Mat t) {
        Mat y = predict(x);//yが毎回同じ
        int[] yy = y.argmax(1);
        int[] tt = t.argmax(1);
        int count = 0;
        for (int i = 0; i < yy.length; i++) {
            if (yy[i] == tt[i]) {
                count++;
            }
        }
        return count / (float) x.shape()[0];
    }

    public float non_layersAccuracy(Mat x, Mat t) {
        Mat y = non_layersPredict(x);
        int[] yy = y.argmax(1);
        int[] tt = t.argmax(1);
        int count = 0;
        for (int i = 0; i < yy.length; i++) {
            if (yy[i] == tt[i]) {
                count++;
            }
        }
        return count / (float) x.shape()[0];
    }

    public int[] validation(Mat x){
        Mat y = non_layersPredict(x);
        int[] yy = y.argmax(1);
        return yy;
    }

}

interface Layer {

    Mat forward(Mat x);

    Mat backward(Mat dout);

}

class Relu implements Layer {

    private boolean[][] mask;

    @Override
    public Mat forward(Mat x) {
        this.mask = x.extract(e -> e <= 0);
        Mat out = new Mat(x.mat);
        out = out.mask(this.mask, 0);
        return out;
    }

    @Override
    public Mat backward(Mat dout) {
        dout = dout.mask(this.mask, 0);
        Mat dx = dout;
        return dx;
    }

}

class Affine implements Layer {

    private Mat w;
    private Vec b;
    private Mat dW;
    private Vec db;
    private Mat x;
    private int[] original_x_shape;

    public Affine(Mat w, Vec b) {
        this.w = w;
        this.b = b;
    }

    @Override
    public Mat forward(Mat x) {
        this.original_x_shape = x.shape();
        x = x.reshape(x.shape()[0], -1);
        this.x = x;
        Mat out = this.x.dot(this.w).add(this.b);
        return out;
    }

    @Override
    public Mat backward(Mat dout) {
        Mat dx = dout.dot(this.w.Transform());
        this.dW = this.x.Transform().dot(dout);
        this.db = dout.sum(0);
        dx = dx.reshape(original_x_shape[0],original_x_shape[1]);
        return dx;
    }

    public Mat getdW() {
        return dW;
    }

    public Vec getdb() {
        return db;
    }

}

class SoftmaxWithLoss {

    private float loss;
    private Mat y;
    private Mat t;

    public float forward(Mat x, Mat t) {
        this.t = t;
        this.y = Functions.softmax(x);
        this.loss = Functions.cross_entropy_error(this.y, this.t);
        return this.loss;
    }

    public Mat backward(float dout) {
        int batch_size = this.t.size();
        Mat dx = null;
        if (this.t.size() == this.y.size()) {//one-hot-vector
            dx = this.y.sub(this.t).div(batch_size);
        }
        return dx;
    }

}

class Functions {

    public static Mat sigmoid(Mat x) {
        return new Mat(x.getRow(), x.getColumn(), 1).div(new Mat(x.getRow(), x.getColumn(), 1).add(x.multi(-1).exp()));
    }

    public static Mat softmax(Mat x) {
        x = x.Transform();
        x = x.sub(x.max(0));
        Mat y = x.exp();
        Vec yy = x.exp().sum(0);
        Mat ret = y.div(yy);
        return ret.Transform();
    }

    public static Mat sigmoid_grad(Mat x) {
        return new Mat(x.getRow(), x.getColumn(), 1).sub(sigmoid(x)).multi(sigmoid(x));
    }

    public static float cross_entropy_error(Mat y, Mat t) {
        int batch_size = y.shape()[0];
        int[] maxIndex = t.argmax(1);
        float[] extVec = new float[batch_size];
        IntStream.range(0, batch_size).parallel().forEach(e -> extVec[e] = y.getMat()[e][maxIndex[e]]);
        IntStream.range(0, batch_size).parallel().forEach(e -> extVec[e] = (float) Math.log(extVec[e]));
        float sum = 0;
        for (float f : extVec) {
            sum += f;
        }
        return -sum / batch_size;
    }

}

class CubeElement {

    public int NDIM;

    public CubeElement() {

    }

    public int getNDIM() {
        return NDIM;
    }

}

class Mat extends CubeElement implements Cloneable {

    private int row;
    private int column;
    public float[][] mat;

    public Mat(float[][] mat) {
        this.mat = mat;
        this.row = mat.length;
        this.column = mat[0].length;
        super.NDIM = 2;
    }

    public Mat(int row, int column) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        super.NDIM = 2;
    }

    public Mat(int row, int column, int num) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> {
            mat[e / column][e % column] = num;
        });
        super.NDIM = 2;
    }

    public Mat(int row, int column, Random random) {
        this.row = row;
        this.column = column;
        mat = new float[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> {
            mat[e / column][e % column] = (float) gaussianRand(random, 0, 1);
        });
        super.NDIM = 2;
    }

    private double gaussianRand(Random rand, double mean, double stdDev) {
        double u1 = 1.0 - rand.nextDouble();
        double u2 = 1.0 - rand.nextDouble();
        double randStdNormal = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2.0 * Math.PI * u2);
        double randNormal = mean + stdDev * randStdNormal;
        return randNormal;
    }

    public Mat add(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] + target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("ADD RANGE ERROR");
            return null;
        }
    }

    public Mat add(Vec target) {
        return add(target.getMat(row));
    }

    public Mat add(float target) {
        return add(new Vec(column, target));
    }

    public Mat sub(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] - target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("SUB RANGE ERROR");
            return null;
        }
    }

    public Mat sub(Vec target) {
        return sub(target.getMat(row));
    }

    public Mat sub(float target) {
        return sub(new Vec(column, target));
    }

    public Mat multi(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] * target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("MULTI RANGE ERROR");
            return null;
        }
    }

    public Mat multi(Vec target) {
        return multi(target.getMat(row));
    }

    public Mat multi(float target) {
        return multi(new Vec(column, target));
    }

    public Mat div(Mat target) {
        if (rangeCheck(this, target)) {
            Mat ret = new Mat(row, column);
            IntStream.range(0, row * column).parallel().forEach(e -> {
                int x = e % column;
                int y = e / column;
                ret.mat[y][x] = mat[y][x] / target.mat[y][x];
            });
            return ret;
        } else {
            System.out.println("DIV RANGE ERROR");
            return null;
        }
    }

    public Mat div(Vec target) {
        return div(target.getMat(row));
    }

    public Mat div(float target) {
        return div(new Vec(column, target));
    }

    public Mat dot(Mat target) {
        if (column == target.row) {
            Mat ret = new Mat(row, target.column);
            IntStream.range(0, ret.row * ret.column).parallel().forEach(e -> {
                int x = e % ret.column;
                int y = e / ret.column;
                float sum = 0;
                for (int i = 0; i < column; i++) {
                    sum += mat[y][i] * target.mat[i][x];
                }
                ret.mat[y][x] = sum;
            });
            return ret;
        } else {
            System.out.println("DOT RANGE ERROR");
            return null;
        }
    }

    public Mat exp() {
        Mat ret = new Mat(row, column);
        IntStream.range(0, row * column).parallel().forEach(e -> {
            int x = e % column;
            int y = e / column;
            ret.mat[y][x] = (float) Math.exp(mat[y][x]);
        });
        //return ret.abs();
        return ret;
    }

    public Mat abs() {
        Mat ret = new Mat(row, column);
        IntStream.range(0, row * column).parallel().forEach(e -> {
            int x = e % column;
            int y = e / column;
            ret.mat[y][x] = (float) Math.abs(mat[y][x]);
        });
        return ret;
    }

    private boolean rangeCheck(Mat a, Mat b) {
        return a.row == b.row && a.column == b.column;
    }

    public Mat Transform() {
        Mat ret = new Mat(column, row);
        IntStream.range(0, column * row).parallel().forEach(e -> {
            ret.mat[e % column][e / column] = mat[e / column][e % column];
        });
        return ret;
    }

    public Mat reshape(int[] arr) {
        return reshape(arr[0], arr[1]);
    }

    public Mat reshape(int newRow, int newColumn) {
        if (newColumn == -1) {
            newColumn = this.row * this.column / newRow;
        }
        float[][] ret = new float[newRow][newColumn];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                int x = (i * mat[i].length + j) % newColumn;
                int y = (i * mat[i].length + j) / newColumn;
                ret[y][x] = mat[i][j];
            }
        }
        return new Mat(ret);
    }

    public boolean[][] extract(Function<Float, Boolean> delegate) {
        boolean[][] ret = new boolean[row][column];
        IntStream.range(0, row * column).parallel().forEach(e -> ret[e / column][e % column] = delegate.apply(mat[e / column][e % column]));
        return ret;
    }

    //maskがtrueの要素はreplaceの値でreplaceする
    public Mat mask(boolean[][] mask, float replace) {
        if (mask.length == row && mask[0].length == column) {
            float[][] ret = new float[row][column];
            IntStream.range(0, row * column).parallel().forEach(e -> ret[e / column][e % column] = mask[e / column][e % column] ? replace : mat[e / column][e % column]);
            return new Mat(ret);
        } else {
            return null;
        }
    }

    @Override
    public Mat clone() {
        Mat ret = null;
        try {
            ret = (Mat) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < row; i++) {
            sb.append("[");
            for (int j = 0; j < column; j++) {
                sb.append(mat[i][j] + (j < column - 1 ? "," : ""));
            }
            sb.append("]" + (i < row - 1 ? "\n" : ""));
        }
        sb.append("]");
        return sb.toString();
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Vec max(int axis) {
        float[] ret = new float[axis % 2 == 0 ? column : row];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            float max = Float.MIN_VALUE;
            for (int i = 0; i < (axis % 2 == 0 ? row : column); i++) {
                float tmp = (axis % 2 == 0 ? mat[i][e] : mat[e][i]);
                if (max < tmp) {
                    max = tmp;
                }
            }
            ret[e] = max;
        });
        return new Vec(ret);
    }

    public Vec sum(int axis) {
        float[] ret = new float[axis % 2 == 1 ? row : column];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            float sum = 0;
            for (int i = 0; i < (axis % 2 == 1 ? column : row); i++) {
                sum += axis % 2 == 1 ? mat[e][i] : mat[i][e];
            }
            ret[e] = sum;
        });
        return new Vec(ret);
    }

    public int[] argmax(int axis) {
        int[] ret = new int[axis % 2 == 1 ? row : column];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            int max = 0;
            for (int i = 0; i < (axis % 2 == 1 ? column : row); i++) {
                float tmp = axis % 2 == 1 ? mat[e][i] : mat[i][e];
                float maxValue = axis % 2 == 1 ? mat[e][max] : mat[max][e];
                if (maxValue < tmp) {
                    max = i;
                }
            }
            ret[e] = max;
        });
        return ret;
    }

    public Mat choice(int[] indexes) {
        float[][] ret = new float[indexes.length][mat[0].length];
        IntStream.range(0, indexes.length).parallel().forEach(e -> {
            for (int i = 0; i < ret[0].length; i++) {
                ret[e][i] = mat[indexes[e]][i];
            }
        });
        return new Mat(ret);
    }

    public int[] shape() {
        int[] ret = {row, column};
        return ret;
    }

    public int size() {
        return row * column;
    }

    public float[][] getMat() {
        return mat;
    }

}

class Vec extends CubeElement implements Cloneable {

    public float[] vec;

    public Vec(float[] vec) {
        this.vec = vec;
        super.NDIM = 1;
    }

    public Vec(int column) {
        this.vec = new float[column];
    }

    public Vec(int column, float num) {
        this.vec = new float[column];
        IntStream.range(0, vec.length).parallel().forEach(e -> vec[e] = num);
    }

    public Mat getMat(int row) {
        float[][] ret = new float[row][vec.length];
        IntStream.range(0, row * vec.length).parallel().forEach(e -> ret[e / vec.length][e % vec.length] = vec[e % vec.length]);
        return new Mat(ret);
    }

    public Mat add(Mat target) {
        return getMat(target.getRow()).add(target);
    }

    public Vec add(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] + target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec add(float target) {
        return add(new Vec(vec.length, target));
    }

    public Mat sub(Mat target) {
        return getMat(target.getRow()).sub(target);
    }

    public Vec sub(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] - target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec sub(float target) {
        return sub(new Vec(vec.length, target));
    }

    public Mat multi(Mat target) {
        return getMat(target.getRow()).multi(target);
    }

    public Vec multi(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] * target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec multi(float target) {
        return multi(new Vec(vec.length, target));
    }

    public Mat div(Mat target) {
        return getMat(target.getRow()).div(target);
    }

    public Vec div(Vec target) {
        if (vec.length != target.vec.length) {
            return null;
        } else {
            float[] ret = new float[vec.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e] / target.vec[e]);
            return new Vec(ret);
        }
    }

    public Vec div(float target) {
        return div(new Vec(vec.length, target));
    }

    public Vec exp() {
        Vec ret = new Vec(vec.length);
        IntStream.range(0, vec.length).parallel().forEach(e -> ret.vec[e] = (float) Math.exp(vec[e]));
        return ret;
    }

    public float max() {
        float n = Float.MIN_VALUE;
        for (float f : vec) {
            if (n < f) {
                n = f;
            }
        }
        return n;
    }

    public float sum() {
        float n = 0;
        for (float f : vec) {
            n += f;
        }
        return n;
    }

    //maskがtrueの要素はreplaceの値でreplaceする
    public Vec mask(boolean[] mask, float replace) {
        if (mask.length == vec.length) {
            float[] ret = new float[mask.length];
            IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = mask[e] ? replace : vec[e]);
            return new Vec(ret);
        } else {
            return null;
        }
    }

    public boolean[] extract(Function<Float, Boolean> delegate) {
        boolean[] ret = new boolean[vec.length];
        IntStream.range(0, vec.length).parallel().forEach(e -> ret[e] = delegate.apply(vec[e]));
        return ret;
    }

    @Override
    public Vec clone() {
        Vec ret = null;
        try {
            ret = (Vec) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public int size() {
        return vec.length;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < vec.length; i++) {
            sb.append(vec[i] + (i == vec.length - 1 ? "" : ","));
        }
        sb.append("]");
        return sb.toString();
    }

}

class AllDelete {

    public static void deleteDirectory(final String dirPath) throws Exception {
        File file = new File(dirPath);
        recursiveDeleteFile(file);
    }

    /**
     * 対象のファイルオブジェクトの削除を行う.<BR>
     * ディレクトリの場合は再帰処理を行い、削除する。
     *
     * @param file ファイルオブジェクト
     * @throws Exception
     */
    private static void recursiveDeleteFile(final File file) throws Exception {
        // 存在しない場合は処理終了
        if (!file.exists()) {
            return;
        }
        // 対象がディレクトリの場合は再帰処理
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                recursiveDeleteFile(child);
            }
        }
        // 対象がファイルもしくは配下が空のディレクトリの場合は削除する
        file.delete();
    }

}

class MNIST {

    private String root;
    private byte[][] trainImage;
    private byte[] trainLabel;
    private byte[][] testImage;
    private byte[] testLabel;

    public MNIST(String root) {
        this.root = root;
        File rf = new File(root);
        if(!rf.exists()) {
            rf.mkdir();
            new MNISTDownloader(root);
        }
        try {
            trainImage = readImage(root + "/" + "train-images-idx3-ubyte");
            trainLabel = readLabel(root + "/" + "train-labels-idx1-ubyte");
            testImage = readImage(root + "/" + "t10k-images-idx3-ubyte");
            testLabel = readLabel(root + "/" + "t10k-labels-idx1-ubyte");
            //AllDelete.deleteDirectory(root);
            //System.out.println("delete temporary files");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        System.out.println(trainImage.length);
        System.out.println(trainLabel.length);
        System.out.println(testImage.length);
        System.out.println(testLabel.length);
    }

    private float[][] normalize(float[][] mat, float normalize) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length] / normalize;
        });
        return ret;
    }

    private int[] convMat(byte[] vec) {
        int[] ret = new int[vec.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e]);
        return ret;
    }

    private float[][] convMat(byte[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = Byte.toUnsignedInt(mat[e / ret[0].length][e % ret[0].length]);
        });
        return ret;
    }

    private float[][] convMat(int[][] mat) {
        float[][] ret = new float[mat.length][mat[0].length];
        IntStream.range(0, ret.length * ret[0].length).parallel().forEach(e -> {
            ret[e / ret[0].length][e % ret[0].length] = mat[e / ret[0].length][e % ret[0].length];
        });
        return ret;
    }

    private int[][] labelVec2Mat(int[] rawLabel) {
        int[][] ret = new int[rawLabel.length][10];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e][rawLabel[e]] = 1);
        return ret;
    }

    private int[] byte2int(byte[] vec) {
        int[] ret = new int[vec.length];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e]);
        return ret;
    }

    private byte[][] readImage(String path) throws IOException {
        //MNISTDownloader.uncompressFile(new File(path + ".gz"));
        int offset = 16;
        int size = 28;
        byte[] vvec = null;
        try {
            vvec = readBin(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //new File(path).delete();
        final byte[] vec = vvec;
        byte[][] ret = new byte[(vec.length - offset) / (size * size)][size * size];
        IntStream.range(0, ret.length).parallel().forEach(e -> {
            for (int i = 0; i < size * size; i++) {
                ret[e][i] = vec[offset + size * size * e + i];
            }
        });
        return ret;
    }

    private byte[] readLabel(String path) throws IOException {
        //MNISTDownloader.uncompressFile(new File(path + ".gz"));
        int offset = 8;
        byte[] vvec = null;
        try {
            vvec = readBin(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //new File(path).delete();
        final byte[] vec = vvec;
        byte[] ret = new byte[vec.length - offset];
        IntStream.range(0, ret.length).parallel().forEach(e -> ret[e] = vec[e + offset]);
        return ret;
    }

    private byte[] readBin(String path) throws IOException {
        return BinaryIO.binaryRead(path);
    }

    public float[][] getTrainImageFloat(float normalized) {
        return normalize(convMat(trainImage), normalized);
    }

    public byte[][] getTrainImageByte() {
        return trainImage;
    }

    public byte[] getTrainLabel() {
        return trainLabel;
    }

    public int[][] getTrainLabelOneHot() {
        return labelVec2Mat(byte2int(trainLabel));
    }

    public float[][] getTrainLabelOneHotFloat() {
        return convMat(labelVec2Mat(byte2int(trainLabel)));
    }

    public float[][] getTestImageFloat(float normalized) {
        return normalize(convMat(testImage), normalized);
    }

    public byte[][] getTestImageByte() {
        return testImage;
    }

    public byte[] getTestLabel() {
        return testLabel;
    }

    public int[][] getTestLabelOneHot() {
        return labelVec2Mat(byte2int(testLabel));
    }

    public float[][] getTestLabelOneHotFloat() {
        return convMat(labelVec2Mat(byte2int(testLabel)));
    }


}

class BinaryIO {

    public static byte[] binaryRead(String path) {
        try {
            byte[] ret = new byte[fileLength(path)];
            byte[] buf = new byte[256];
            FileInputStream input = new FileInputStream(path);
            int len;
            for (int i = 0; (len = input.read(buf)) != -1; i++) {
                for (int j = 0; j < len; j++) {
                    ret[i * buf.length + j] = buf[j];
                }
            }
            input.close();
            return ret;
        } catch (IOException e) {
            return null;
        }
    }

    private static int fileLength(String path) {
        int ret = 0;
        try {
            int len;
            byte[] buf = new byte[256];
            FileInputStream input = new FileInputStream(path);
            while ((len = input.read(buf)) != -1) {
                ret += len;
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private static byte[] vecFix(ArrayList<Byte> vec) {
        byte[] ret = new byte[vec.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = vec.get(i);
        }
        return ret;
    }

    public static void binaryWrite(String path, byte[] bytes) {
        try {
            FileOutputStream output = new FileOutputStream(path);
            output.write(bytes, 0, bytes.length);
            output.flush();
            output.close();
        } catch (IOException e) {

        }
    }
}

class MNISTDownloader {

    protected static final int EOF = -1;
    private String dstDir = "";

    public MNISTDownloader() {
        downloadNIST();
    }

    public MNISTDownloader(String dstDir) {
        this.dstDir = dstDir;
        downloadNIST();
    }

    private void downloadNIST() {
        String[] targets = {"train-images-idx3-ubyte", "train-labels-idx1-ubyte", "t10k-images-idx3-ubyte", "t10k-labels-idx1-ubyte"};
        for (String s : targets) {
            try {
                uncompressFile(downloadWrapper(s));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
    http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
    http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
    http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
    */
    private File downloadWrapper(String fileName) {
        System.out.println("downloading..." + fileName);

        if (download("http://yann.lecun.com/exdb/mnist/" + fileName + ".gz", (dstDir.equals("") ? "" : dstDir + "/") + fileName + ".gz")) {
            return new File((dstDir.equals("") ? "" : dstDir + "/") + fileName + ".gz");
        } else {
            return null;
        }
    }

    private boolean download(String urlStr, String target) {
        if (new File(target).exists()) {
            System.out.println("Skipping...");
            return true;
        } else {
            httpDownloadWrapper(urlStr, target);
            return true;
        }
    }

    public static boolean uncompressFile(File file) throws FileNotFoundException, IOException {
        System.out.println("Extracting...");
        boolean flag = false;
        try {
            BufferedInputStream bis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(file)));
            int n = file.getPath().length();
            String bodyName = file.getPath().substring(0, n - 3);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(bodyName)));

            int c;
            while ((c = bis.read()) != EOF) {
                bos.write((byte) c);
            }
            bis.close();
            bos.close();
            flag = true;
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
        return flag;
    }

    private void httpDownloadWrapper(String src, String dst) {
        try {

            URL url = new URL(src);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setAllowUserInteraction(false);
            conn.setInstanceFollowRedirects(true);
            conn.setRequestMethod("GET");
            conn.connect();

            int httpStatusCode = conn.getResponseCode();

            if (httpStatusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception();
            }

            // Input Stream
            DataInputStream dataInStream
                    = new DataInputStream(
                    conn.getInputStream());

            // Output Stream
            DataOutputStream dataOutStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));

            // Read Data
            byte[] b = new byte[4096];
            int readByte = 0;

            while (-1 != (readByte = dataInStream.read(b))) {
                dataOutStream.write(b, 0, readByte);
            }

            // Close Stream
            dataInStream.close();
            dataOutStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

