import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends JFrame {

    private static String currentAlgo = "none";
    private ModelManager model = new ModelManager("/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam");

    private String currentMode = "TRAIN mode";

    private final String[] combodata = {"RED", "BLUE", "GREEN", "YELLOW", "BLACK", "WHITE"};
    private String currentTrainTarget = "RED";

    public static void main(String[] args) {
        Main frame = new Main("タイトル");
        frame.setVisible(true);
    }

    Main(String title) {

        //基本設定

        final int imageOffset = 20;
        final int menuWidth = 110;

        setTitle(title);
        setBounds(50, 50, 900, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel menuPane = new JPanel();
        menuPane.setPreferredSize(new Dimension(menuWidth, 100));
        menuPane.setBackground(Color.BLUE);

        JPanel imagePane = new JPanel();
        imagePane.setBackground(Color.BLACK);

        JLabel imageLabel = new JLabel(model.getModels()[0].getImageIcon());


        imagePane.add(imageLabel);
        imagePane.setPreferredSize(new Dimension(model.getModels()[0].getImageIcon().getIconWidth(), model.getModels()[0].getImageIcon().getIconHeight()));

        Container contentPane = getContentPane();
        contentPane.add(menuPane, BorderLayout.WEST);
        contentPane.add(imagePane, BorderLayout.CENTER);

        //menu設定

        JPanel algoSelectPane = new JPanel();
        algoSelectPane.setPreferredSize(new Dimension(menuWidth, 130));
        algoSelectPane.setBackground(Color.RED);

        JButton openButton = new JButton("Open");
        openButton.setPreferredSize(new Dimension(menuWidth, 20));
        openButton.addActionListener(e->{
            JFileChooser filechooser = new JFileChooser();
            filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int selected = filechooser.showOpenDialog(this);
            if (selected == JFileChooser.APPROVE_OPTION) {
                File file = filechooser.getSelectedFile();
                model=new ModelManager(file.getAbsolutePath());
                imageLabel.setIcon(model.getModels()[0].getImageIcon());
            } else if (selected == JFileChooser.CANCEL_OPTION) {
                System.out.println("キャンセルされました");
            } else if (selected == JFileChooser.ERROR_OPTION) {
                System.out.println("エラー又は取消しがありました");
            }
        });

        JButton saveButton = new JButton("Save");
        saveButton.setPreferredSize(new Dimension(menuWidth, 20));
        saveButton.addActionListener(e->{
            JFileChooser filechooser = new JFileChooser();
            int selected = filechooser.showSaveDialog(this);
            if (selected == JFileChooser.APPROVE_OPTION) {
                File file = filechooser.getSelectedFile();
                model.writeLog(file.getAbsolutePath()+".txt");
            } else if (selected == JFileChooser.CANCEL_OPTION) {
                System.out.println("キャンセルされました");
            } else if (selected == JFileChooser.ERROR_OPTION) {
                System.out.println("エラー又は取消しがありました");
            }
        });

        JToggleButton buttonSVM = new JToggleButton("SVM");
        buttonSVM.setPreferredSize(new Dimension(menuWidth, 20));

        JToggleButton buttonCorrelation = new JToggleButton("Correlation");
        buttonCorrelation.setPreferredSize(new Dimension(menuWidth, 20));

        JToggleButton buttonNN = new JToggleButton("NearestNeighbor");
        buttonNN.setPreferredSize(new Dimension(menuWidth, 20));


        buttonSVM.addActionListener(e->{
            buttonSVM.setSelected(true);
            buttonCorrelation.setSelected(false);
            buttonNN.setSelected(false);
            currentAlgo=e.getActionCommand();
        });
        buttonCorrelation.addActionListener(e->{
            buttonSVM.setSelected(false);
            buttonCorrelation.setSelected(true);
            buttonNN.setSelected(false);
            currentAlgo=e.getActionCommand();
        });
        buttonNN.addActionListener(e->{
            buttonSVM.setSelected(false);
            buttonCorrelation.setSelected(false);
            buttonNN.setSelected(true);
            currentAlgo=e.getActionCommand();
        });

        algoSelectPane.add(openButton);
        algoSelectPane.add(saveButton);
        algoSelectPane.add(buttonSVM);
        algoSelectPane.add(buttonCorrelation);
        algoSelectPane.add(buttonNN);

        //train設定
        JPanel trainPane = new JPanel();
        trainPane.setPreferredSize(new Dimension(menuWidth, 600));
        trainPane.setBackground(Color.ORANGE);

        JButton modeButton = new JButton("TRAIN mode");
        modeButton.setPreferredSize(new Dimension(menuWidth, 20));
        modeButton.addActionListener(e->{
            String actionCommand = e.getActionCommand();
            switch (actionCommand){
                case "TRAIN mode":
                    currentMode = "TEST mode";
                    modeButton.setText(currentMode);
                    break;
                case "TEST mode":
                    currentMode = "TRAIN mode";
                    modeButton.setText(currentMode);
                    break;
            }
        });

        JComboBox fileSelector = new JComboBox(model.getFileNames());
        fileSelector.setFont(new Font("consolas", Font.PLAIN, 11));

        JComboBox trainSelector = new JComboBox(combodata);

        JButton undo = new JButton("Undo");
        undo.setPreferredSize(new Dimension(menuWidth, 20));


        JTextArea selectInfo = new JTextArea("");
        selectInfo.setFont(new Font("consolas", Font.PLAIN, 11));
        selectInfo.setPreferredSize(new Dimension(menuWidth,450));

        trainPane.add(modeButton);
        trainPane.add(undo);
        trainPane.add(fileSelector);
        trainPane.add(trainSelector);
        trainPane.add(selectInfo);

        menuPane.add(algoSelectPane);
        menuPane.add(trainPane);

        Runnable dispUpdate = () ->{
            HashMap<String, ArrayList<PixInfo>> trainData = model.getModels()[fileSelector.getSelectedIndex()].getTrainData();
            StringBuilder sb = new StringBuilder();
            BufferedImage dispImage = null;
            if(trainData.containsKey(currentTrainTarget)) {
                for (PixInfo pi : trainData.get(currentTrainTarget)) {
                    sb.append(pi.toString() + "\n");
                }
                model.getModels()[fileSelector.getSelectedIndex()].addImageMarker(trainData.get(currentTrainTarget));
            }
            imageLabel.setIcon(model.getModels()[fileSelector.getSelectedIndex()].getImageIcon());
            selectInfo.setText(sb.toString());
        };

        //selectInfoを使うので、定義後にlistener設置
        //画像がクリックされたとき
        imageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(currentMode.equals("TEST mode")){
                    //MLManagerに当該ピクセル情報を渡し、モーダルで判定結果を表示する
                }else {
                    HashMap<String, ArrayList<PixInfo>> trainData = model.getModels()[fileSelector.getSelectedIndex()].getTrainData();
                    if (!trainData.containsKey(currentTrainTarget)) {
                        trainData.put(currentTrainTarget, new ArrayList<>());
                    }
                    trainData.get(currentTrainTarget).add(new PixInfo(e.getX(), e.getY(), model.getModels()[fileSelector.getSelectedIndex()].getImage().getRGB(e.getX(), e.getY())));
                    dispUpdate.run();
                }
            }
        });

        //対象ファイルが変更されたとき
        fileSelector.addItemListener(
                new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        dispUpdate.run();
                    }
                }
        );

        //学習対象が変更されたとき
        trainSelector.addActionListener(
                e->{
                    String s = (String)trainSelector.getItemAt(trainSelector.getSelectedIndex());
                    currentTrainTarget=s;

                    HashMap<String, ArrayList<PixInfo>> trainData = model.getModels()[fileSelector.getSelectedIndex()].getTrainData();
                    if(!trainData.containsKey(currentTrainTarget)){
                        trainData.put(currentTrainTarget,new ArrayList<>());
                    }
                    dispUpdate.run();
                });

        //Undoボタンが押されたとき
        undo.addActionListener(e->{
            HashMap<String, ArrayList<PixInfo>> trainData = model.getModels()[fileSelector.getSelectedIndex()].getTrainData();
            if(trainData.containsKey(currentTrainTarget)){
                if(0<trainData.get(currentTrainTarget).size()) {
                    trainData.get(currentTrainTarget).remove(trainData.get(currentTrainTarget).size() - 1);
                }
            }
            dispUpdate.run();
        });


        setBounds(50, 50, model.getModels()[fileSelector.getSelectedIndex()].getImageIcon().getIconWidth() + imageOffset + menuWidth, model.getModels()[fileSelector.getSelectedIndex()].getImageIcon().getIconHeight()+100);
    }

}

class PixInfo{

    private int x;
    private int y;
    private int color;

    public PixInfo(int x,int y,int color){
        this.x=x;
        this.y=y;
        this.color=color;
    }

    private int r(int c) {
        return c >> 16 & 0xff;
    }

    private int g(int c) {
        return c >> 8 & 0xff;
    }

    private int b(int c) {
        return c & 0xff;
    }

    public String toString(){
        return "(x,y)=("+x+","+y+")\n(r,g,b)=("+r(color)+","+g(color)+","+b(color)+")\n==========================";
    }

    public int getX(){
        return x;
    }

    public int getY() {
        return y;
    }
}

/**
 * 複数のModelを管理する
 */
class ModelManager{

    private String dirPath;
    private String[] datasetPathes;
    private Model[] models;

    /**
     * コンストラクタ
     * @param dirPath :データセットが格納されているディレクトリのパス
     */
    public ModelManager(String dirPath){
        this.dirPath=dirPath;
        this.datasetPathes=makeDatasetPathes(dirPath);
        this.models = new Model[datasetPathes.length];
        for(int i=0;i<models.length;i++){
            models[i]=new Model(datasetPathes[i]);
        }
    }

    private String[] makeDatasetPathes(String dirPath){
        ArrayList<String> ret =new ArrayList<>();
        for(File f:new File(dirPath).listFiles()){
            if(f.getAbsolutePath().contains(".jpg")){
                ret.add(f.getAbsolutePath());
            }
        }
        return fixVec(ret);
    }

    private String[] fixVec(ArrayList<String> vec){
        String[] ret = new String[vec.size()];
        for(int i=0;i<ret.length;i++){
            ret[i]=vec.get(i);
        }
        return ret;
    }

    public Model[] getModels() {
        return models;
    }

    public String[] getFileNames(){
        String[] ret = new String[datasetPathes.length];
        for(int i=0;i<ret.length;i++){
            ret[i]=new File(datasetPathes[i]).getName();
        }
        return ret;
    }

    public void writeLog(String path){
        StringBuilder sb = new StringBuilder();
        for(Model model:models){
            sb.append(model.toString());
        }
        try {
            FileWriter fw = new FileWriter(new File(path));
            fw.write(sb.toString());
            fw.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}

class Model {

    private String currentImagePath;
    private BufferedImage image;
    private ImageIcon imageIcon;

    private HashMap<String,ArrayList<PixInfo>> trainData;

    public Model(String path) {
        setCurrentImagePath(path);
        trainData = new HashMap<>();
    }

    public void setCurrentImagePath(String path) {
        this.currentImagePath = path;
        this.image = imageReadWrapper(currentImagePath);
        this.imageIcon = new ImageIcon(currentImagePath);
    }

    private BufferedImage imageReadWrapper(String path) {
        BufferedImage ret = null;
        try {
            ret = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public void addImageMarker(ArrayList<PixInfo> pixInfos){
        BufferedImage newIconImage = copyImage(image);
        final int markerSize = 10;
        for(PixInfo pi:pixInfos){
            for(int i=0;i<markerSize;i++){
                try{
                    newIconImage.setRGB(pi.getX()-i,pi.getY(),rgb(255,0,0));
                    newIconImage.setRGB(pi.getX()+i,pi.getY(),rgb(255,0,0));
                    newIconImage.setRGB(pi.getX(),pi.getY()-i,rgb(255,0,0));
                    newIconImage.setRGB(pi.getX(),pi.getY()+i,rgb(255,0,0));
                }catch(ArrayIndexOutOfBoundsException e){
                    //do nothing
                }
            }
        }
        imageIcon = new ImageIcon(newIconImage);
    }

    private int rgb(int r, int g, int b) {
        return 0xff000000 | r << 16 | g << 8 | b;
    }

    private static BufferedImage copyImage(BufferedImage src){
        BufferedImage dist=new BufferedImage(
                src.getWidth(), src.getHeight(), src.getType());
        dist.setData(src.getData());
        return dist;
    }

    public String getCurrentImagePath() {
        return currentImagePath;
    }

    public BufferedImage getImage() {
        return image;
    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public HashMap<String, ArrayList<PixInfo>> getTrainData() {
        return trainData;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(currentImagePath+"\n");
        for(Map.Entry<String,ArrayList<PixInfo>> map:trainData.entrySet()){
            sb.append("<"+map.getKey()+">\n");
            for(PixInfo pi:map.getValue()){
                sb.append(pi.toString()+"\n");
            }
        }
        return sb.toString();
    }
}

//GIMPに色域選択機能があるので、これを使って色域ごとのマスク画像を作成しSVMの学習データにする
class MachineLearningManager{

    public MachineLearningManager(){

    }

}

