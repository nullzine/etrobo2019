import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args){
        new Main().run();
    }

    private void run(){

    }

    private BufferedImage readImageWrapper(String path){
        BufferedImage ret = null;
        try{
            ret = ImageIO.read(new File(path));
        }catch(IOException e){
            e.printStackTrace();
        }
        return ret;
    }

}
