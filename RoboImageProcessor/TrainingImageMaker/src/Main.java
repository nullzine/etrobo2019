import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {
        String output = "/Users/nullzine/Git/etrobo2019/RoboImageProcessor/etRingTrainImage";
        for(String s:DatasetPool.moriokaANetworkWebCam()){
            for(CutArea cutarea:new CutAreaMapper().getCutMap()){
                if(cutarea.isSettingEnable(s)){
                    cutarea.cutImage(s,output);
                }
            }
        }
    }

}

class CutAreaMapper{

    private ArrayList<CutArea> cutMap;

    public CutAreaMapper(){
        cutMap = new ArrayList<>();
        /*
        cutMap.add(new CutArea(
                new Area[]{
                        new Area(new Point(0,0),new Point(0,0)),
                        new Area(new Point(0,0),new Point(0,0))},
                "dataset_20190629_144913.jpg",
                "dataset_20190629_145446.jpg",
                "dataset_20190629_150850.jpg",
                "dataset_20190629_151305.jpg",
                "dataset_20190629_151843.jpg",
                "dataset_20190629_152311.jpg",
                "dataset_20190629_152735.jpg",
                "dataset_20190629_154329.jpg",
                "dataset_20190629_154755.jpg"
        ));
        cutMap.add(new CutArea(
                new Area(),
                "WIN_20190727_14_14_46_Pro.jpg",
                "WIN_20190727_14_14_53_Pro.jpg",
                "WIN_20190727_14_15_32_Pro.jpg",
                "WIN_20190727_14_16_51_Pro.jpg",
                "WIN_20190727_14_17_23_Pro.jpg",
                "WIN_20190727_14_17_47_Pro.jpg",
                "WIN_20190727_14_18_17_Pro.jpg",
                "WIN_20190727_14_18_40_Pro.jpg",
                "WIN_20190727_14_19_16_Pro.jpg"
        ));
        cutMap.add(new CutArea(
                new Area(),
                "WIN_20190727_14_20_17_Pro.jpg",
                "WIN_20190727_14_20_45_Pro.jpg",
                "WIN_20190727_14_21_12_Pro.jpg",
                "WIN_20190727_14_21_38_Pro.jpg",
                "WIN_20190727_14_22_19_Pro.jpg",
                "WIN_20190727_14_22_46_Pro.jpg",
                "WIN_20190727_14_55_02_Pro.jpg"
        ));
        cutMap.add(new CutArea(
                new Area(),
                "WIN_20190727_14_55_17_Pro.jpg"
        ));
        cutMap.add(new CutArea(
                new Area(),
                "WIN_20190727_14_55_43_Pro.jpg",
                "WIN_20190727_14_55_43_Pro.jpg",
                "WIN_20190727_14_56_44_Pro.jpg",
                "WIN_20190727_14_57_01_Pro.jpg",
                "WIN_20190727_14_57_16_Pro.jpg",
                "WIN_20190727_14_57_34_Pro.jpg",
                "WIN_20190727_14_57_47_Pro.jpg",
                "WIN_20190727_14_58_04_Pro.jpg"
        ));
        */
        cutMap.add(new CutArea(
                new Area[]{
                        new Area(396,144,57,49),
                        new Area(661,114,47,45),
                        new Area(834,156,57,28),
                        new Area(776,222,65,32),
                        new Area(1038,181,62,25),
                        new Area(1011,252,79,42),
                        new Area(448,273,77,34),
                        new Area(690,316,79,39),
                        new Area(977,359,93,55),
                        new Area(53,340,92,41),
                        new Area(270,392,97,51)
                },
                "dataset_20190727_151714.jpg"
        ));
        cutMap.add(new CutArea(
                new Area[]{
                        new Area(746,36,56,24),
                        new Area(934,54,59,24),
                        new Area(1151,72,74,30),
                        new Area(484,75,57,27),
                        new Area(659,99,64,26),
                        new Area(871,123,69,32),
                        new Area(1131,121,74,68),
                        new Area(346,149,70,34),
                        new Area(543,183,71,31),
                        new Area(778,220,85,41),
                        new Area(1079,267,101,52),
                        new Area(163,251,82,40),
                        new Area(366,299,95,55),
                        new Area(643,364,107,64),
                        new Area(1012,446,137,89)
                },
                "dataset_20190727_151757.jpg",
                "dataset_20190727_153959.jpg",
                "dataset_20190727_154021.jpg",
                "dataset_20190727_154042.jpg",
                "dataset_20190727_154102.jpg",
                "dataset_20190727_154206.jpg",
                "dataset_20190727_154231.jpg",
                "dataset_20190727_154255.jpg"
        ));
    }

    public ArrayList<CutArea> getCutMap() {
        return cutMap;
    }
}

class DatasetPool {

    public static String[] totoDataset() {
        return ("/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_144913.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_145446.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_150850.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_151305.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_151843.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_152311.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_152735.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_154329.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/camSysDataset/7/dataset_20190629_154755.jpg").split("\n");
    }

    public static String[] moriokaADirectWebCam() {
        return ("/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_14_46_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_14_53_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_15_32_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_16_51_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_17_23_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_17_47_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_18_17_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_18_40_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_19_16_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_20_17_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_20_45_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_21_12_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_21_38_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_22_19_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_22_46_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_55_02_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_55_17_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_55_43_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_56_44_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_57_01_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_57_16_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_57_34_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_57_47_Pro.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/direct_webcam/WIN_20190727_14_58_04_Pro.jpg").split("\n");
    }

    public static String[] moriokaANetworkWebCam() {
        return ("/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_151714.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_151757.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_153959.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154021.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154042.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154102.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154206.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154231.jpg\n" +
                "/Users/nullzine/Git/etrobo2019/moriokaDatasetA/network_webcam/dataset_20190727_154255.jpg").split("\n");
    }


}

class CutArea{

    private Area[] areas;
    private String[] targetPathes;

    public CutArea(Area[] areas,String...tagetPathes){
        this.areas=areas;
        this.targetPathes=tagetPathes;
    }

    public boolean isSettingEnable(String absPath){
        String target = new File(absPath).getName();
        for(String s:targetPathes){
            if(s.equals(target)){
                return true;
            }
        }
        return false;
    }

    public void cutImage(String path,String output){
        for(Area area:areas){
            String target = "";
            for(int i=0;true;i++){
                target = output+"/"+new File(path).getName()+".result_"+i+".png";
                if(!new File(target).exists()){
                    break;
                }
            }
            BufferedImage tmp = imageReadWrapper(path).getSubimage(area.start.x,area.start.y,area.getW(),area.getH());
            imageWriteWrapper(target,tmp);
        }
    }

    private BufferedImage imageReadWrapper(String path){
        BufferedImage ret = null;
        try{
            ret = ImageIO.read(new File(path));
        }catch (IOException e){
            System.out.println(path);
            e.printStackTrace();
        }
        return ret;
    }

    private void imageWriteWrapper(String path,BufferedImage target){
        try{
            ImageIO.write(target,"png",new File(path));
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}

class Area{

    public Point start;
    public Point end;

    public Area(int x,int y,int w,int h){
        this.start=new Point(x,y);
        this.end=new Point(x+w,y+h);
    }

    public Area(Point start,Point end){
        this.start=start;
        this.end=end;
    }

    public int getW(){
        return end.x-start.x;
    }

    public int getH(){
        return end.y-start.y;
    }

}

class Point{

    public int x;
    public int y;

    public Point(int x,int y){
        this.x=x;
        this.y=y;
    }

}



