# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt

class Pair:

    def __init__(self,first,second):
        self.first=first
        self.second=second

def MatchConfidence(matcher,query,train,confidence):
    knn_Matches = []
    matcher.knnMatch(query,train,knn_Matches,2)


def lowerBound(vec,target):
    for i in range(len(vec)):
        if vec[i].first<target.first:
            return i
    return 0

def CrossCheckMatches(query,train):
    train_indices = []
    for dm in train:
        p = Pair(dm.trainIdx,dm.queryIdx)
        insertPoint = lowerBound(train_indices,p)
        train_indices.insert(insertPoint,p)

    dst = []
    for q in query:
        p = Pair(q.queryIdx,q.trainIdx)
        if p in train_indices:
            dst.append(p)
    return dst

def CrossMatch(fst_descriptor,snd_descriptor,confidence):
    matcher = cv2.BFMatcher_create(cv2.NORM_L2,crossCheck=True)

    fst_matches = MatchConfidence(matcher,fst_descriptor,snd_descriptor,confidence)

    snd_matches = MatchConfidence(matcher,snd_descriptor,fst_descriptor,confidence)

    dst_matches = CrossCheckMatches(fst_matches,snd_matches)

    return dst_matches

def run():
    print("Start")
    dir_path = "C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\akazeTest"
    fst_filename = "WIN_20190727_14_55_43_Pro.jpg"
    snd_filename = "dataset_20190629_144431.jpg"
    output_basename = "match_points"
    certainty = 0.6

    fst_image_path = dir_path+"\\"+fst_filename
    fst_image = cv2.imread(fst_image_path,cv2.IMREAD_COLOR)

    snd_image_path = dir_path+"\\"+snd_filename
    snd_image = cv2.imread(snd_image_path,cv2.IMREAD_COLOR)

    # A-KAZE検出器の生成
    detector = cv2.AKAZE_create()

    # 特徴量の検出と特徴量ベクトルの計算
    fst_keypoints,fst_descriptor = detector.detectAndCompute(fst_image, None)
    snd_keypoints,snd_descriptor = detector.detectAndCompute(snd_image, None)

    matches = CrossMatch(fst_descriptor,snd_descriptor,certainty)


'''
    # Brute-Force Matcher生成
    bf = cv2.BFMatcher()

    # 特徴量ベクトル同士をBrute-Force＆KNNでマッチング
    matches = bf.knnMatch(fst_descriptor, snd_descriptor, k=2)

    # データを間引きする
    ratio = 0.6
    good = []
    for m, n in matches:
        if m.distance < ratio * n.distance:
            good.append([m])

    # 対応する特徴点同士を描画
    result = cv2.drawMatchesKnn(fst_image, fst_keypoints, snd_image, snd_keypoints, good, None, flags=2)

    # 画像表示
    cv2.imshow('img', result)
    cv2.imwrite('akaze_result.jpg', result)

    # キー押下で終了
    cv2.waitKey(0)
    cv2.destroyAllWindows()
'''



run()