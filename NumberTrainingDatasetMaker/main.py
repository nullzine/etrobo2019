import cv2
import numpy as np
import os

class DataSet:

    @classmethod
    def getTGUReferenceImageR(cls):
        return cv2.imread("../CameraColorPickSystem/TGURefImageR.jpg", cv2.IMREAD_COLOR)

    @classmethod
    def getTGUReferenceImageL(cls):
        return cv2.imread("../CameraColorPickSystem/TGURefImageL.jpg",cv2.IMREAD_COLOR)

    @classmethod
    def getMoriokaReferenceImageR(cls):
        return cv2.imread("../CameraColorPickSystem/MoriokaRefImageR.jpg",cv2.IMREAD_COLOR)

    @classmethod
    def getMoriokaReferenceImageL(self):
        return cv2.imread("../CameraColorPickSystem/MoriokaRefImageL.jpg",cv2.IMREAD_COLOR)

    @classmethod
    def getTGUNumberCropPointR(cls):
        return ("R",[[1118,328],[1240,397],[1093,670],[828,449]])

    @classmethod
    def getTGUNumberCropPointL(cls):
        return ("L",[[45,395],[154,325],[464,463],[175,700]])

    @classmethod
    def getMoriokaNumberCropPointR(cls):
        return ("R",[[1060,305],[1169,361],[1033,616],[782,423]])

    @classmethod
    def getMoriokaNumberCropPointL(cls):
        return ("L",[[95,357],[206,308],[487,427],[220,611]])

    @classmethod
    def getCropTarget(cls,type,size):
        if type=="L":
            return [[size/2,0],[size,0],[size,size],[0,size]]
        else:
            return [[0,0],[size/2,0],[size,size],[0,size]]



class Akaze:

    @classmethod
    def MatchConfidence(cls,matcher, query, train, confidence):
        # 特徴量ベクトル同士をBrute-Force＆KNNでマッチング
        matches = matcher.knnMatch(query, train, k=2)

        # データを間引きする
        good = []
        for m, n in matches:
            if m.distance < confidence * n.distance:
                good.append(m)
        return good

    @classmethod
    def CrossCheck(cls,matches1, matches2):
        s = set()
        for d in matches2:
            s.add((d.trainIdx, d.queryIdx))
        good = []
        for d in matches1:
            if (d.queryIdx, d.trainIdx) in s: good.append(d)
        return good

    @classmethod
    def CrossMatch(cls,query, train, confidence):
        # Brute-Force Matcher生成
        bf = cv2.BFMatcher()

        query_matches = cls.MatchConfidence(bf, query, train, confidence)
        train_matches = cls.MatchConfidence(bf, train, query, confidence)
        return cls.CrossCheck(query_matches, train_matches)

    @classmethod
    # slave画像をmaster画像に合わせこむ
    def akazeProcessor(cls,master, slave):
        fst_image = slave

        snd_image = master

        # A-KAZE検出器の生成
        detector = cv2.AKAZE_create()

        # 特徴量の検出と特徴量ベクトルの計算
        kp1, des1 = detector.detectAndCompute(fst_image, None)
        kp2, des2 = detector.detectAndCompute(snd_image, None)

        good = cls.CrossMatch(des1, des2, 0.6)
        print("size of good matches : " + str(len(good)))

        # 対応する特徴点同士を描画
        result = cv2.drawMatches(fst_image, kp1, snd_image, kp2, good, None, flags=2)

        img3 = None

        if len(good) > 10:
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 2.0)
            matchesMask = mask.ravel().tolist()

            h, w, _ = fst_image.shape
            pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)

            img2 = cv2.polylines(snd_image, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
            cv2.imwrite('resultVisualize.jpg', img2)

            size = tuple(np.array([snd_image.shape[1], snd_image.shape[0]]))
            img3 = cv2.warpPerspective(fst_image, M, dsize=size)
            cv2.imwrite('result.jpg', img3)

        else:
            print
            "Not enough matches are found - %d/%d" % (len(good), 10)
            matchesMask = None

        return img3,len(good)

class AddImageNoise:

    # ヒストグラム均一化
    @classmethod
    def equalizeHistRGB(cls,src):
        RGB = cv2.split(src)
        Blue = RGB[0]
        Green = RGB[1]
        Red = RGB[2]
        for i in range(3):
            cv2.equalizeHist(RGB[i])

        img_hist = cv2.merge([RGB[0], RGB[1], RGB[2]])
        return img_hist

    # ガウシアンノイズ
    @classmethod
    def addGaussianNoise(cls,src):
        row, col, ch = src.shape
        mean = 0
        var = 0.1
        sigma = 15
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gauss = gauss.reshape(row, col, ch)
        noisy = src + gauss

        return noisy

    # salt&pepperノイズ
    @classmethod
    def addSaltPepperNoise(cls,src):
        row, col, ch = src.shape
        s_vs_p = 0.5
        amount = 0.004
        out = src.copy()
        # Salt mode
        num_salt = np.ceil(amount * src.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                  for i in src.shape]
        out[coords[:-1]] = (255, 255, 255)

        # Pepper mode
        num_pepper = np.ceil(amount * src.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                  for i in src.shape]
        out[coords[:-1]] = (0, 0, 0)
        return out


class Util:

    @classmethod
    def find_all_files(cls,directory):
        for root, dirs, files in os.walk(directory):
            yield root
            for file in files:
                yield os.path.join(root, file)

    @classmethod
    def imageListMaker(cls,path):
        ret = []
        for file in cls.find_all_files(path):
            root, ext = os.path.splitext(file)
            if ext == ".jpg":
                ret.append(file)
        return ret

    @classmethod
    def correctionAndCrop(cls,target):
        src = cv2.imread(target,cv2.IMREAD_COLOR)
        #最適な合わせこみ対象を探索する
        akazeReferences = [DataSet.getTGUReferenceImageL(),DataSet.getTGUReferenceImageR(),DataSet.getMoriokaReferenceImageL(),DataSet.getMoriokaReferenceImageR()]
        cropPoints = [DataSet.getTGUNumberCropPointL(),DataSet.getTGUNumberCropPointR(),DataSet.getMoriokaNumberCropPointL(),DataSet.getMoriokaNumberCropPointR()]
        maxPointImage, maxPoint = Akaze.akazeProcessor(DataSet.getTGUReferenceImageL(), src)
        maxCropPoint = cropPoints[0]
        for idx in range(len(akazeReferences)):
            reference = akazeReferences[idx]
            cp = cropPoints[idx]
            pointImage,point = Akaze.akazeProcessor(reference, src)
            if maxPoint < point:
                maxPointImage = pointImage
                maxPoint = point
                maxCropPoint = cp

        #数宇切り出し
        size = 280
        useCPType,useCP = maxCropPoint
        dst_pts = np.array(useCP, dtype=np.float32)
        src_pts = np.array(DataSet.getCropTarget(useCPType,size), dtype=np.float32)
        mat_i = cv2.getPerspectiveTransform(dst_pts, src_pts)
        if not maxPointImage is None:
            perspective_img = cv2.warpPerspective(maxPointImage, mat_i, (size, size))
            return perspective_img
        else:
            return None





def run():
    outputDir = "/Users/nullzine/work/etrobo/akazeOut"

    dataList = Util.imageListMaker("../camSysDataset")
    dataList.extend(Util.imageListMaker("../FixPointMeasurementL"))
    dataList.extend(Util.imageListMaker("../moriokaDatasetA"))
    for e in dataList:
        print(e)
        result = [Util.correctionAndCrop(e)]
        if not result[0] is None:
            result.append(AddImageNoise.addGaussianNoise(result[0]))
            result.append(AddImageNoise.addSaltPepperNoise(result[0]))
            result.append(AddImageNoise.addSaltPepperNoise(result[0]))
        for idx,image in enumerate(result):
            cv2.imwrite(outputDir+"/"+str(idx)+"_"+os.path.basename(e), image)


run()