# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt

class Color():
    # 色コード
    UNKNOWN = 0
    RED     = 1
    GREEN   = 2
    BLUE    = 3
    BLACK   = 4
    YELLOW  = 5

    # 色コードから表示用文字列への変換メソッド
    def toColorName(code):
        COLOR_NAME = {
            Color.UNKNOWN: "Unknown",
            Color.RED:     "RED",
            Color.GREEN:   "GREEN",
            Color.BLUE:    "BLUE",
            Color.BLACK:   "BLACK",
            Color.YELLOW:  "YELLOW"
        }
        return COLOR_NAME[code]

    # HSV値から色コードへの変換メソッド(閾値は環境に合わせて調整下さい)
    def getColor(hsv):
        if (0 <= hsv[2] and hsv[2] <= 20):
            return Color.BLACK
        if (0 <= hsv[0] and hsv[0] <= 15) \
            and (20 <= hsv[1] and hsv[1] <= 255) \
            and (20 <= hsv[2] and hsv[2] <= 255):
            return Color.RED
        if (100 <= hsv[0] and hsv[0] <= 115) \
            and (60 <= hsv[1] and hsv[1] <= 255) \
            and (60 <= hsv[2] and hsv[2] <= 255):
            return Color.BLUE
        if (45 <= hsv[0] and hsv[0] <= 90) \
            and (50 <= hsv[1] and hsv[1] <= 255) \
            and (50 <= hsv[2] and hsv[2] <= 255):
            return Color.GREEN
        if (20 <= hsv[0] and hsv[0] <= 30) \
            and (20 <= hsv[1] and hsv[1] <= 255) \
            and (20 <= hsv[2] and hsv[2] <= 255):
            return Color.YELLOW
        return Color.UNKNOWN

def MatchConfidence(matcher,query,train,confidence):
    # 特徴量ベクトル同士をBrute-Force＆KNNでマッチング
    matches = matcher.knnMatch(query, train, k=2)

    # データを間引きする
    good = []
    for m, n in matches:
        if m.distance < confidence * n.distance:
            good.append(m)
    return good

def CrossCheck(matches1, matches2):
    s = set()
    for d in matches2:
        s.add((d.trainIdx, d.queryIdx))
    good = []
    for d in matches1:
        if (d.queryIdx, d.trainIdx) in s: good.append(d)
    return good

def CrossMatch(query, train, confidence):
    # Brute-Force Matcher生成
    bf = cv2.BFMatcher()

    query_matches = MatchConfidence(bf, query, train, confidence)
    train_matches = MatchConfidence(bf, train, query, confidence)
    return CrossCheck(query_matches, train_matches)

#slave画像をmaster画像に合わせこむ
def akazeProcessor(master,slave=None,img=None):
    fst_image_path = slave
    fst_image = cv2.imread(fst_image_path, cv2.IMREAD_COLOR)
    if slave == None:
        fst_image = img

    snd_image_path = master
    snd_image = cv2.imread(snd_image_path, cv2.IMREAD_COLOR)

    # A-KAZE検出器の生成
    detector = cv2.AKAZE_create()

    # 特徴量の検出と特徴量ベクトルの計算
    kp1, des1 = detector.detectAndCompute(fst_image, None)
    kp2, des2 = detector.detectAndCompute(snd_image, None)

    good = CrossMatch(des1, des2, 0.6)
    print("size of good matches : " + str(len(good)))

    # 対応する特徴点同士を描画
    result = cv2.drawMatches(fst_image, kp1, snd_image, kp2, good, None, flags=2)

    if len(good) > 10:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 2.0)
        matchesMask = mask.ravel().tolist()

        h, w, _ = fst_image.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)

        img2 = cv2.polylines(snd_image, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
        cv2.imwrite('resultVisualize.jpg', img2)

        size = tuple(np.array([snd_image.shape[1], snd_image.shape[0]]))
        img3 = cv2.warpPerspective(fst_image, M, dsize=size)
        cv2.imwrite('result.jpg', img3)

    else:
        print
        "Not enough matches are found - %d/%d" % (len(good), 10)
        matchesMask = None

    return img3

def detectColor(img):
    dots = {1: (38, 135), 2: (265, 90), 3: (456, 51), 4: (616, 19), 5: (72, 209), 6: (338, 149), 7: (552, 98),
            8: (727, 63), 9: (123, 321), 10: (439, 232), 11: (680, 165), 12: (875, 111), 13: (204, 506), 14: (595, 363),
            15: (878, 255), 16: (1086, 194)}

    result = []

    for (x, y) in dots.values():
        # RGBからHSVへ変換
        pixelValue = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[y, x]

        # 色判定
        col = Color.getColor(pixelValue)  # HSVから色コードへ変換
        result.append((x, y, col))  # (x座標,y座標,色)タプルをリストへ追加

        # 色情報を画像へ付加
        dot_num = [k for k, v in dots.items() if (x == v[0] and y == v[1])][0]
        H = str(pixelValue[0])
        S = str(pixelValue[1])
        V = str(pixelValue[2])
        cv2.circle(img, (x, y), 3, (255, 255, 255), -1)
        cv2.putText(img, ("[%d]:" % dot_num) + Color.toColorName(col), (x + 10, y + 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    (255, 255, 255), 1, cv2.LINE_AA)
        cv2.putText(img, H + "," + S + "," + V, (x + 10, y + 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1,
                    cv2.LINE_AA)
    return img

def run():
    print("Start")
    #dir_path = "C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\akazeTest"
    fst_filename = "C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\akazeTest\\WIN_20190727_14_55_43_Pro.jpg"
    #snd_filename = "dataset_20190629_144431.jpg"
    snd_filename = "C:\\Users\\nullzine\\Documents\\Git\\etrobo2019\\akazeTest\\gkR.jpg"
    output_basename = "match_points"

    # ローカルカメラのデバイスID
    DEVICE_ID = 0

    # Raspberry Pi3（カメラシステム）のURL
    # url = "http://192.168.11.100:8080/?action=stream"
    url = "http://192.168.11.100/?action=stream"  # 本番

    # VideoCaptureのインスタンスを作成する。
    cap = cv2.VideoCapture(url)  # カメラシステムを使う場合
    #cap = cv2.VideoCapture(DEVICE_ID)  # このpythonスクリプトを実行するPCのカメラを使う場合コメントアウトして下さい

    # カメラFPSを30FPSに設定
    cap.set(cv2.CAP_PROP_FPS, 30)

    # カメラ画像の横幅を1280に設定
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)

    # カメラ画像の縦幅を720に設定
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

    # ピクセル配列をゼロ初期化
    #img = np.zeros((720, 1280, 3), dtype=np.uint8)

    if cap.isOpened():

        while True:
            # 画像をキャプチャ
            ret, input = cap.read()
            img = input
            #img = akazeProcessor(snd_filename,fst_filename)
            img = akazeProcessor(snd_filename, None,img)
            img = detectColor(img)
            cv2.imshow('frame', img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    cap.release()






run()