# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt


def MatchConfidence(matcher,query,train,matches,confidence):
    if matches==None:
        return
    knn_Matches = []
    cv2.DescriptorMatcher.knnMatch()

#slave画像をmaster画像に合わせこむ
def akazeProcessor(master,slave):
    fst_image_path = slave
    fst_image = cv2.imread(fst_image_path, cv2.IMREAD_COLOR)

    snd_image_path = master
    snd_image = cv2.imread(snd_image_path, cv2.IMREAD_COLOR)

    # A-KAZE検出器の生成
    detector = cv2.AKAZE_create()

    # 特徴量の検出と特徴量ベクトルの計算
    kp1, des1 = detector.detectAndCompute(fst_image, None)
    kp2, des2 = detector.detectAndCompute(snd_image, None)

    # Brute-Force Matcher生成
    bf = cv2.BFMatcher()

    # 特徴量ベクトル同士をBrute-Force＆KNNでマッチング
    matches = bf.knnMatch(des1, des2, k=2)

    # データを間引きする
    ratio = 0.6
    goodMatch = []
    good = []
    for m, n in matches:
        if m.distance < ratio * n.distance:
            good.append(m)
            goodMatch.append([m])

    # 対応する特徴点同士を描画
    result = cv2.drawMatchesKnn(fst_image, kp1, snd_image, kp2, goodMatch, None, flags=2)

    if len(good) > 10:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()

        h, w, _ = fst_image.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)

        img2 = cv2.polylines(snd_image, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)
        cv2.imwrite('resultVisualize.jpg', img2)

        size = tuple(np.array([snd_image.shape[1], snd_image.shape[0]]))
        img3 = cv2.warpPerspective(fst_image, M, dsize=size)
        cv2.imwrite('result.jpg', img3)

    else:
        print
        "Not enough matches are found - %d/%d" % (len(good), 10)
        matchesMask = None

    return img3

def run():
    print("Start")
    dir_path = "C:\\Users\\nullzine\\PycharmProjects\\etrobo2019\\akazeTest"
    fst_filename = "WIN_20190727_14_55_43_Pro.jpg"
    snd_filename = "dataset_20190629_144431.jpg"
    output_basename = "match_points"

    result = akazeProcessor(dir_path+"\\"+snd_filename,dir_path+"\\"+fst_filename)

    # 画像表示
    cv2.imshow('img', result)
    cv2.imwrite('akaze_result.jpg', result)

    # キー押下で終了
    cv2.waitKey(0)
    cv2.destroyAllWindows()






run()